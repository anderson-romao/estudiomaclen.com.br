<?
$h1         = 'Videos';
$title      = 'Videos';
$desc       = 'Videos';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Videos';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h2 class="my-4">Incorporações de vídeos</h2>

				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hA6hldpSTF8" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>

				<hr class="cinza my-5">

				<h2>Proporções de telas podem ser personalizadas com classes modificadoras</h2>

				<div class="row">
					<div class="col">
						<p>Proporção 21:9</p>
						<div class="embed-responsive embed-responsive-21by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hA6hldpSTF8" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
					
					<div class="col">
						<p>Proporção 16:9</p>
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hA6hldpSTF8" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				
				<hr class="cinza">

				<div class="row">
					<div class="col">
						<p>Proporção 4:3</p> 
						<div class="embed-responsive embed-responsive-4by3">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hA6hldpSTF8" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
					
					<div class="col">
						<p>Proporção 1:1</p> 
						<div class="embed-responsive embed-responsive-1by1">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hA6hldpSTF8" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>