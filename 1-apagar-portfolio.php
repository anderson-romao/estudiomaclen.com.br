<?
$h1         = 'Portfolio';
$title      = 'Portfolio';
$desc       = 'Portfolio';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Portfolio';
include('inc/head.php');
?>

<style>
/* ------- Modelo 1 ------- */
#filters li span {
	cursor: pointer;
}
#filters li span.active {
	background: #e95a44;
	color:#fff;
}
#portfoliolist .portfolio {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-o-box-sizing: border-box;
	width:23%;
	margin:5px;
	display:none;
	overflow:hidden;
}
.portfolio-wrapper {
	overflow:hidden;
	position: relative !important;
	cursor:pointer;
}
.portfolio img {
	max-width:100%;
}
/*  Mobile (Portrait) - Note: Design for a width of 320px */
@media only screen and (max-width: 767px) {
	#portfoliolist .portfolio {
		width:45%;
		margin:5px;
	}		
}
</style>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>

			<article>
				<h2 class="my-4">Modelo 1</h2>

				<div class="text-center">
					<ul id="filters" class="nav justify-content-center my-4 clearfix">
						<li class="nav-item"><span class="filter py-2 px-3 active" data-filter=".app, .cartoes, .icon, .logo, .web">Todos</span></li>
						<li class="nav-item"><span class="filter py-2 px-3" data-filter=".app">App</span></li>
						<li class="nav-item"><span class="filter py-2 px-3" data-filter=".cartoes">Cartões</span></li>
						<li class="nav-item"><span class="filter py-2 px-3" data-filter=".icon">Icones</span></li>
						<li class="nav-item"><span class="filter py-2 px-3" data-filter=".logo">Logos</span></li>
						<li class="nav-item"><span class="filter py-2 px-3" data-filter=".web">Web</span></li>
					</ul>
				</div>
				
				<div id="portfoliolist">

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">				
							<img src="http://via.placeholder.com/400x300?text=Logo+5" alt="">

							<!-- Caso seja necessário uma descrição descomentar linha a baixo -->
							
							<!-- <div class="label">
								<div class="label-text">
									<a class="text-title">Bird Document</a>
									<span class="text-category">Logo</span>
								</div>
								<div class="label-bg"></div>
							</div> -->
							
						</div>
					</div>				

					<div class="portfolio app" data-cat="app">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=App+1" alt="">
						</div>
					</div>		

					<div class="portfolio web" data-cat="web">
						<div class="portfolio-wrapper">						
							<img src="http://via.placeholder.com/400x300?text=Web+4" alt="">
						</div>
					</div>				

					<div class="portfolio cartoes" data-cat="cartoes">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Cartao+1" alt="">
						</div>
					</div>	

					<div class="portfolio app" data-cat="app">
						<div class="portfolio-wrapper">
							<img src="http://via.placeholder.com/400x300?text=App+3" alt="">
						</div>
					</div>			

					<div class="portfolio cartoes" data-cat="cartoes">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Cartao+4" alt="">
						</div>
					</div>	

					<div class="portfolio cartoes" data-cat="cartoes">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Cartao+5" alt="">
						</div>
					</div>	

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Logo+1" alt="">
						</div>
					</div>																

					<div class="portfolio app" data-cat="app">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=App+2" alt="">
						</div>
					</div>														

					<div class="portfolio cartoes" data-cat="cartoes">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Cartao+2" alt="">
						</div>
					</div>				

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Logo+6" alt="">
						</div>
					</div>																	

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Logo+7" alt="">
						</div>
					</div>													

					<div class="portfolio icon" data-cat="icon">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Icone+4" alt="">
						</div>
					</div>							

					<div class="portfolio web" data-cat="web">
						<div class="portfolio-wrapper">						
							<img src="http://via.placeholder.com/400x300?text=Web+3" alt="">
						</div>
					</div>	

					<div class="portfolio icon" data-cat="icon">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Icone+1" alt="">
						</div>
					</div>				

					<div class="portfolio web" data-cat="web">
						<div class="portfolio-wrapper">						
							<img src="http://via.placeholder.com/400x300?text=Web+2" alt="">
						</div>
					</div>																	

					<div class="portfolio icon" data-cat="icon">
						<div class="portfolio-wrapper">
							<img src="http://via.placeholder.com/400x300?text=Icone+2" alt="">
						</div>
					</div>																			

					<div class="portfolio icon" data-cat="icon">
						<div class="portfolio-wrapper">						
							<img src="http://via.placeholder.com/400x300?text=Icone+5" alt="">
						</div>
					</div>			

					<div class="portfolio web" data-cat="web">
						<div class="portfolio-wrapper">						
							<img src="http://via.placeholder.com/400x300?text=Web+1" alt="">
						</div>
					</div>									

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Logo+3" alt="">
						</div>
					</div>																	

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Logo+4" alt="">
						</div>
					</div>

					<div class="portfolio icon" data-cat="icon">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Icone+3" alt="">
						</div>
					</div>

					<div class="portfolio cartoes" data-cat="cartoes">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Cartao+3" alt="">
						</div>
					</div>	

					<div class="portfolio logo" data-cat="logo">
						<div class="portfolio-wrapper">			
							<img src="http://via.placeholder.com/400x300?text=Logo+2" alt="">
						</div>
					</div>				

				</div>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

	<!---------- Modelo 1 ---------->
	<script>
		$(function () {
			var filterList = {
				init: function () {
					// MixItUp plugin
					// http://mixitup.io
					$('#portfoliolist').mixItUp({
	  				selectors: {
	    			  target: '.portfolio',
	    			  filter: '.filter'	
	    		  },
	    		  // load: {
	      		//   filter: '.app' //Essa opção permite que um grupo apareça primeiro
	      		// }     
					});								
				}
			};
			// Run the show!
			filterList.init();
		});	
	</script>

</body>