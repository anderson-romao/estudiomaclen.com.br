<li class="nav-item mx-2">
	<a href="<?=$url?>" title="Home" class="nav-link d-md-inline-block">Home</a>
</li>
<li class="nav-item mx-2">
	<a href="<?=$url?>empresa" title="Empresa" class="nav-link d-md-inline-block">Empresa</a>
</li>
<li class="nav-item dropdown mx-2">
	<a href="<?=$url?>servicos" title="Serviços" class="nav-link d-md-inline-block" data-toggle="dropdown">Serviços</a>
	<ul class="dropdown-menu dropdown-menu-estilo">
		<?include('inc/sub-menu-servicos.php');?>
	</ul>
</li>
<li class="nav-item mx-2">
	<a href="<?=$url?>projetos-realizados" title="Projetos Realizados" class="nav-link d-md-inline-block">Projetos Realizados</a>
</li>
<li class="nav-item mx-2">
	<a href="https://lojaintegrada.com.br/" target="_blank" rel="nofollow" title="Loja Virtual" class="nav-link d-md-inline-block">Loja Virtual</a>
</li>
<li class="nav-item dropdown">
	<a href="<?=$url?>blog" title="Blog" class="nav-link d-md-inline-block" data-toggle="dropdown">Blog</a>
	<ul class="dropdown-menu dropdown-menu-estilo">
		<li><a href="#" title="Sub-menu blog" class="dropdown-item">Sub-menu blog</a></li>
		<li><a href="#" title="Sub-menu blog" class="dropdown-item">Sub-menu blog</a></li>
		<li><a href="#" title="Sub-menu blog" class="dropdown-item">Sub-menu blog</a></li>
		<li><a href="#" title="Sub-menu blog" class="dropdown-item">Sub-menu blog</a></li>
		<li><a href="#" title="Sub-menu blog" class="dropdown-item">Sub-menu blog</a></li>
		<li><a href="#" title="Sub-menu blog" class="dropdown-item">Sub-menu blog</a></li>
	</ul>
</li>

<li class="nav-item mx-2">
	<a href="<?=$url?>contato" title="Contato" class="nav-link d-md-inline-block">Contato</a>
</li>

<li class="nav-item dropdown">
	<a href="<?=$url?>#" title="Exemplos" class="nav-link d-md-inline-block" data-toggle="dropdown">Exemplos</a>
	<ul class="dropdown-menu dropdown-menu-estilo">
		<?include('inc/sub-menu.php');?>
	</ul>
</li>

<!-- <li class="nav-item mx-2">
	<a href="<?=$url?>saiba-mais" class="nav-link d-md-inline-block">Saiba mais</a>
	<ul class="dropdown-menu dropdown-menu-estilo">
		<?#include('inc/sub-menu-saiba-mais.php');?>
	</ul>
</li> -->