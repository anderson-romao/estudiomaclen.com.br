<div class="carousel-03">
  <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselIndicators" data-slide-to="0" class="icons-circle active"></li>
      <li data-target="#carouselIndicators" data-slide-to="1" class="icons-circle"></li>
      <li data-target="#carouselIndicators" data-slide-to="2" class="icons-circle"></li>
      <li data-target="#carouselIndicators" data-slide-to="3" class="icons-circle"></li>
      <li data-target="#carouselIndicators" data-slide-to="4" class="icons-circle"></li>
      <li data-target="#carouselIndicators" data-slide-to="5" class="icons-circle"></li>
      <li data-target="#carouselIndicators" data-slide-to="6" class="icons-circle"></li>
    </ol>
    <div class="carousel-inner" role="listbox">

      <div class="txt-fixed h-100 w-100 d-flex">
        <div class="my-auto mx-auto text-center text-white">
          <h2 class="display-3 font-weight-bold text-shadow"><?=$nomeSite?></h2>
          <h3 class="text-shadow">Trazemos luxo para a classe C</h3>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light shadow mt-3">Saiba mais</a>
        </div>
      </div>

      <div class="carousel-item banner-01 active">
        <div class="carousel-caption d-none d-md-block text-white">
          <!-- <h2 class="display-4 font-weight-bold">Primeiro Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a> -->
        </div>
      </div>

      <!-- <div class="carousel-item banner-02">
        <div class="carousel-caption d-none d-md-block text-white">
          <h2 class="display-4 font-weight-bold">Segundo Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a>
        </div>
      </div> -->

      <div class="carousel-item banner-03">
        <div class="carousel-caption d-none d-md-block text-white">
          <!-- <h2 class="display-4 font-weight-bold">Terceiro Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a> -->
        </div>
      </div>

      <div class="carousel-item banner-04">
        <div class="carousel-caption d-none d-md-block text-white">
          <!-- <h2 class="display-4 font-weight-bold">Quarto Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a> -->
        </div>
      </div>

      <div class="carousel-item banner-05">
        <div class="carousel-caption d-none d-md-block text-white">
          <!-- <h2 class="display-4 font-weight-bold">Quinto Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a> -->
        </div>
      </div>

      <div class="carousel-item banner-06">
        <div class="carousel-caption d-none d-md-block text-white">
          <!-- <h2 class="display-4 font-weight-bold">Sexto Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a> -->
        </div>
      </div>

      <div class="carousel-item banner-07">
        <div class="carousel-caption d-none d-md-block text-white">
          <!-- <h2 class="display-4 font-weight-bold">Sétimo Slide</h2>
          <p class="lead mb-5">Lorem ipsum dolor sit amet, consectetur.</p>
          <a href="#" title="ESCREVA_AQUI" class="btn btn-light my-5">Saiba mais</a> -->
        </div>
      </div>

      <a class="carousel-control-prev txt-fixed" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="fas fa-angle-left fa-2x text-white bg-secondary p-2 px-3 rounded-circle" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
      </a>
      <a class="carousel-control-next txt-fixed" href="#carouselIndicators" role="button" data-slide="next">
        <span class="fas fa-angle-right fa-2x text-white bg-secondary p-2 px-3 rounded-circle" aria-hidden="true"></span>
        <span class="sr-only">Próximo</span>
      </a>
      
    </div>
  </div>
</div>