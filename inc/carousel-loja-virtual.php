<div class="jumbotron jumbotron-fluid mb-0">
  <div class="container text-center px-4">
    <h2>Loja virtual</h2>
    <p class="px-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem quam eius sint ipsam facere cumque, ullam vero odio, possimus itaque iusto sequi. Beatae, esse earum modi sed dignissimos similique, est.</p>

    <div class="customer-cards slider py-5">
      
      <div class="slide">
        <a href="<?=$url?>#" title="Acessórios">
          <img src="<?=$url?>imagens/img-loja-virtual/acessorios.png" alt="Acessórios" title="Acessórios" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Acessórios" class="text-secondary border-bottom border-secondary">Acessórios</a> -->
        </div>
      </div>
      
      <div class="slide">
        <a href="<?=$url?>#" title="Almofadas">
          <img src="<?=$url?>imagens/img-loja-virtual/almofadas-01.png" alt="Almofadas" title="Almofadas" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Almofadas" class="text-secondary border-bottom border-secondary">Almofadas</a> -->
        </div>
      </div>

      <div class="slide">
        <a href="<?=$url?>#" title="Almofadas">
          <img src="<?=$url?>imagens/img-loja-virtual/almofadas-02.png" alt="Almofadas" title="Almofadas" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Almofadas" class="text-secondary border-bottom border-secondary">Almofadas</a> -->
        </div>
      </div>
      
      <div class="slide">
        <a href="<?=$url?>#" title="Almofadas">
          <img src="<?=$url?>imagens/img-loja-virtual/almofadas-03.png" alt="Almofadas" title="Almofadas" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Almofadas" class="text-secondary border-bottom border-secondary">Almofadas</a> -->
        </div>
      </div>

      <div class="slide">
        <a href="<?=$url?>#" title="Luminária">
          <img src="<?=$url?>imagens/img-loja-virtual/luminaria-01.png" alt="Luminária" title="Luminária" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Luminária" class="text-secondary border-bottom border-secondary">Luminária</a> -->
        </div>
      </div>
      
      <div class="slide">
        <a href="<?=$url?>#" title="Luminária">
          <img src="<?=$url?>imagens/img-loja-virtual/luminaria-02.png" alt="Luminária" title="Luminária" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Luminária" class="text-secondary border-bottom border-secondary">Luminária</a> -->
        </div>
      </div>

      <div class="slide">
        <a href="<?=$url?>#" title="Mantas">
          <img src="<?=$url?>imagens/img-loja-virtual/manta.png" alt="Mantas" title="Mantas" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Mantas" class="text-secondary border-bottom border-secondary">Mantas</a> -->
        </div>
      </div>
      
      <div class="slide">
        <a href="<?=$url?>#" title="Quadros e vasos">
          <img src="<?=$url?>imagens/img-loja-virtual/quadro-e-vaso.png" alt="Quadros e vasos" title="Quadros e vasos" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Quadros e vasos" class="text-secondary border-bottom border-secondary">Quadros e vasos</a> -->
        </div>
      </div>

      <div class="slide">
        <a href="<?=$url?>#" title="Quadros">
          <img src="<?=$url?>imagens/img-loja-virtual/quadros.png" alt="Quadros" title="Quadros" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Quadros" class="text-secondary border-bottom border-secondary">Quadros</a> -->
        </div>
      </div>
      
      <div class="slide">
        <a href="<?=$url?>#" title="Tapetes">
          <img src="<?=$url?>imagens/img-loja-virtual/tapetes.png" alt="Tapetes" title="Tapetes" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Tapetes" class="text-secondary border-bottom border-secondary">Tapetes</a> -->
        </div>
      </div>

      <div class="slide">
        <a href="<?=$url?>#" title="Vasinhos">
          <img src="<?=$url?>imagens/img-loja-virtual/vasinhos.png" alt="Vasinhos" title="Vasinhos" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Vasinhos" class="text-secondary border-bottom border-secondary">Vasinhos</a> -->
        </div>
      </div>
      
      <div class="slide">
        <a href="<?=$url?>#" title="Vasos">
          <img src="<?=$url?>imagens/img-loja-virtual/vasos.png" alt="Vasos" title="Vasos" class="card-img-top">
        </a>
        <div class="card-body text-center">
          <!-- <a href="<?=$url?>#" title="Vasos" class="text-secondary border-bottom border-secondary">Vasos</a> -->
        </div>
      </div>
      
    </div>
  </div>
</div>