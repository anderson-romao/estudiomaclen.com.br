<div id="carousel-indicador" class="carousel slide" data-ride="carousel">
	
	<ol class="carousel-indicators">
		<li data-target="carousel-indicador" data-slide-to="0" class="active"></li>
		<li data-target="carousel-indicador" data-slide-to="1"></li>
		<li data-target="carousel-indicador" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="holder.js/800x400?auto=yes&bg=777&fg=555&text=Primeiro Slide" alt="Primeiro Slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="holder.js/800x400?auto=yes&bg=777&fg=555&text=Segundo Slide" alt="Segundo Slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="holder.js/800x400?auto=yes&bg=777&fg=555&text=Terceiro Slide" alt="Terceiro Slide">
		</div>
	</div>

	<a href="carousel-indicador" class="carousel-control-prev" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Anterior</span>
	</a>
	<a href="carousel-indicador" class="carousel-control-next" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Próximo</span>
	</a>

</div>