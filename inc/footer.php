<!-- footer -->
<footer class="container-fluid bg-banner-footer pt-4 pb-5">
	<div class="container">
		<div class="row mb-5">
			<div class="col-12 col-md-1">
				<img src="imagens/logo.png" alt="Logo <?=$h1?>" class="rounded-circle" width="100" height="100">
			</div>

			<div class="col-12 col-md-10">
				<nav class="menu-footer">
					<ul class="nav justify-content-center">
						<li class="mobile-center">
							<a href="<?=$url?>" title="Home" class="nav-link text-white">Home</a>
						</li>
						<li class="mobile-center">
							<a href="<?=$url?>empresa" title="Empresa" class="nav-link text-white">Empresa</a>
						</li>
						<li class="mobile-center">
							<a href="<?=$url?>servicos" title="Serviços" class="nav-link text-white">Serviços</a>
						</li>
						<li class="mobile-center">
							<a href="<?=$url?>projetos-realizados" title="Projetos Realizados" class="nav-link text-white">Projetos Realizados</a>
						</li>
						<li class="mobile-center">
							<a href="https://lojaintegrada.com.br/" target="_blank" rel="nofollow" title="Loja Virtual" class="nav-link text-white">Loja Virtual</a>
						</li>
						<li class="mobile-center">
							<a href="<?=$url?>blog" title="Blog" class="nav-link text-white">Blog</a>
						</li>
						<li class="mobile-center">
							<a href="<?=$url?>contato" title="Contato" class="nav-link text-white">Contato</a>
						</li>
						<li class="mobile-center">
							<a href="<?=$url?>mapa-site" class="nav-link text-white">Mapa do site</a>
						</li>
					</ul>
				</nav>
			</div>

			<div class="col-12 col-md-1">
				<?include('inc/redes-sociais.php') ?>
			</div>
		</div>
	</div>	
</footer>
<div class="bg-dark">
	<div class="container text-white-50">
		<div class="row">
			<div class="col-12 col-md">
				<p class="text-center small my-2">Desenvolvido por: <a href="<?=$siteCreditos?>" title="Desenvolvido por" target="_blank" rel="nofollow" class="text-white-50"><?=$creditos?></a></p>
			</div>
			<div class="col-12 col-md">
				<ul class="list-inline text-center text-md-right my-1">
					<li class="list-inline-item">
						<a class="text-white-50 small" href="http://validator.w3.org/check?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5 fa-2x"></i></a>
					</li>
					<li class="list-inline-item">
						<a class="text-white-50 small" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C"><i class="fab fa-css3-alt fa-2x"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- srcoll top -->
<button id="buttonTop" class="btn shadow rounded-circle bg-secondary"><i class="text-light fas fa-arrow-up"></i></button>

<!-- JavaScript (Opcional) -->
<!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
    
<!-- funções separadas em javascript  -->
<script src="js/funcoes.js"></script>

<!-- FancyBox -->
<script src="js/fancybox/jquery-3.2.1.min.js"></script>
<script src="js/fancybox/jquery.fancybox.min.js"></script>

<!-- Portfolio (Pagina Galeria.php) -->
<script src="js/jquery.mixitup.min.js"></script>

<!-- WhatsHelp.io widget -->
<?include('inc/whatsapp.php');?>

<!-- checklist dados e description -->
<?#include('inc/checklist.php');?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 

<script src="js/jquery.homeycombs.js"></script> 
<script>
    $(document).ready(function() {
        $('.honeycombs').honeycombs();
    });
</script>
<script>
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>