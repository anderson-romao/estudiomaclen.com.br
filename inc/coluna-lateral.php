<div class="col-12 col-md-4">
    <aside class="border border-secondary rounded px-4 py-3">
        <h4 class="text-center border-bottom border-secondary pb-2">
            <a href="<?=$url?>saiba-mais" class="text-secondary" title="Saiba mais <?=$nomeSite?>">Saiba mais</a>
        </h4>
        <nav>
            <ul>
                <? include('inc/sub-menu.php');?>
            </ul>
        </nav>
        <?php
        if(isset($fone2) || isset($fone3)){
        echo "<p class='text-center h5'>Entre em contato pelos telefones</p>";
        } else {
        echo "<p class='text-center h5'>Entre em contato pelo telefone</p>";
        }
        ?>
    </aside>
    <div class="text-center mt-2">
        <button href="contato" title="Solicite um orçamento" class="btn btn-secondary btn-block">Solicite um orçamento</button>
    </div>
</div>