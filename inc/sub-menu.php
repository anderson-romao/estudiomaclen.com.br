<li><a href="1-apagar-menu" class="dropdown-item">Menus Modelos</a></li>
<li><a href="1-apagar-footer" class="dropdown-item">Footer Modelos</a></li>
<li><a href="1-apagar-banner-tela-completa" class="dropdown-item">Banner Tela Completa</a></li>

<li class="dropdown-submenu">
	<a href="1-apagar-breadcrumb" class="dropdown-item dropdown-toggle">Breadcrumb</a>
	<ul class="dropdown-menu dropdown-menu-estilo">
		<li><a href="1-apagar-breadcrumb-2" class="dropdown-item">Breadcrumb 2</a></li>
	</ul>
</li>

<li><a href="1-apagar-linhas" class="dropdown-item">Modelo de Linhas</a></li>
<li><a href="1-apagar-cards" class="dropdown-item">Cards</a></li>

<li class="dropdown-submenu">
	<a href="#" class="dropdown-item dropdown-toggle">Midia</a>
	<ul class="dropdown-menu dropdown-menu-estilo">
		<li><a href="1-apagar-imagens" class="dropdown-item" title="Iamgens">Imagens</a></li>
		<li><a href="1-apagar-videos" class="dropdown-item" title="Vídeos">Vídeos</a></li>
		<li><a href="1-apagar-fancybox" class="dropdown-item" title="Fancybox">Fancybox</a></li>
	</ul>
</li>
<li><a href="1-apagar-carousel" class="dropdown-item">Carousel</a></li>
<li><a href="1-apagar-portfolio" class="dropdown-item">Portfolio</a></li>
<li><a href="1-apagar-linha-do-tempo" class="dropdown-item">Linha do tempo</a></li>
<li><a href="1-apagar-one-page" class="dropdown-item">Modelo One Page</a></li>
<li><a href="1-apagar-contador" class="dropdown-item">Contador</a></li>
<li><a href="1-apagar-thumbnails" class="dropdown-item">Thumbnails</a></li>
<!-- <li><a href="1-apagar-collapse" class="dropdown-item">Collapse</a></li> -->