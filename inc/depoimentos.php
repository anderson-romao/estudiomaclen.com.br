<div class="container text-center px-4">
  <h2>Depoimentos de nossos clientes</h2>

  <div class="depoimentos slider">

    <div class="slide">
      <div class="card-body px-0 px-md-5">
        <img src="<?=$url?>imagens/depoimento-01.jpg" alt="Depoimento" title="Depoimento" width="150" class="rounded-circle mb-3">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo id perspiciatis sed, sapiente consectetur rerum tempora illo quisquam, minus, ullam aut reprehenderit cupiditate? Veniam amet harum inventore temporibus voluptates tenetur!</p>
      </div>
    </div>

    <div class="slide">
      <div class="card-body px-0 px-md-5">
        <img src="<?=$url?>imagens/depoimento-02.jpg" alt="Depoimento" title="Depoimento" width="150" class="rounded-circle mb-3">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo voluptas, accusantium nam dolor. A officia sapiente molestias ad nostrum numquam, quam, consectetur at ipsam veniam, unde mollitia beatae! Saepe, voluptatum!</p>
      </div>
    </div>

    <div class="slide">
      <div class="card-body px-0 px-md-5">
        <img src="<?=$url?>imagens/depoimento-03.jpg" alt="Depoimento" title="Depoimento" width="150" class="rounded-circle mb-3">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id placeat iusto, quo ipsa beatae. Ab odio amet soluta repellat, inventore!</p>
      </div>
    </div>

  </div>
</div>
