<div class="jumbotron jumbotron-fluid">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 text-center text-md-left">
				<h2 class="text-uppercase font-weight-bold">Projetos realizados</h2>
				<p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis sapiente fugiat deserunt possimus dolorem deleniti aliquam! Fuga quam, provident numquam?</p>
				<a href="<?=$url?>projetos-realizados" class="btn bg-dark text-white" title="Projetos 3D">conheça</a>
			</div>

			<div class="col-12 col-md-12">
				
				<div class="honeycombs">
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-01.jpg"/>
						<span>Projeto<br>Quarto 1</span>
					</div>
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-02.jpg"/>
						<span>Projeto<br>Quarto 2</span>
					</div>
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-03.jpg"/>
						<span>Projeto<br>Quarto 3</span>
					</div>
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-04.jpg"/>
						<span>Projeto<br>Quarto 4</span>
					</div>
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-05.jpg"/>
						<span>Projeto<br>Quarto 5</span>
					</div>
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-06.jpg"/>
						<span>Projeto<br>Quarto 6</span>
					</div>
					<div class="comb">
						<img src="imagens/projetos-realizados/projeto-07.jpg"/>
						<span>Projeto<br>Quarto 7</span>
					</div>
					<div class="comb mb-0 pb-0">
						<img src="imagens/projetos-realizados/projeto-08.jpg"/>
						<span>Projeto<br>Quarto 8</span>
					</div>
				</div>

			</div>
		</div>	
	</div>
</div>
