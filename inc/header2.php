<header>
	<nav class="navbar fixed-top sticky-top navbar-expand-md navbar-dark bg-dark text-white">
	
		<div class="container">
			<a href="<?=$url?>" class="navbar-brand">
				<img src="imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
				<?=$nomeSite?>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSite">
				<ul class="navbar-nav py-2">
					<? include('inc/menu.php'); ?>
				</ul>
			</div>
		</div>
		
	</nav>
</header>