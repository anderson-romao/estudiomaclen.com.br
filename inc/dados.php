
<?
ini_set("display_errors", "on");

$nomeSite			= 'Estúdio Maclen';
$slogan				= 'Consultoria de interiores';
$url				= 'http://localhost/estudiomaclen.com.br/';
// $url				= 'http://aff.net.br/teste/estudiomaclen.com.br/';
// $url				= 'http:www.estudiomaclen.com.br/';
$ddd				= '11';
$fone				= '95943-0781';
// $fone2				= '2222-4444';
// $fone3				= '2123-4444';

//Se não tiver whatsapp, remover scrip da footer 
$whatsapp			= '+55(11)95943-0781';
$infoWhats			= 'Entre em contato!';
//Se não tiver whatsapp, remover scrip da footer 

$ServidorSMTP		= 'smtp.gmail.com'; //smtp.gmail.com (gmail) 
$emailContato		= 'estudio.maclen@gmail.com';
$senha				= 'senha-aqui';

//$rua				= 'xxxxxxxx, 999';
//$bairro				= 'qqqqqqqqqqq';
//$cidade				= 'São Paulo';
//$UF					= 'SP';
//$cep				= 'CEP: 99999-555';
//$latitude			= '-13.702797';
//$longitude			= '-69.6865109';
//$idCliente			= 'ID do cliente';
//$mapa				= '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3876.258344559582!2d-69.6886995851701!3d-13.702796990379124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTPCsDQyJzEwLjEiUyA2OcKwNDEnMTEuNCJX!5e0!3m2!1spt-BR!2sbr!4v1545179653171" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';

$creditos			= 'Anderson Romão | Front End Developer';
$siteCreditos		= 'https://andersonromao.dev/';

$author = '';

//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = strpos($urlhtaccess, 'http://www.') === false ? 'http://' : 'http://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess,'/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');
//Fim Gerar htaccess automático

$breadcrumb = '
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent my-2 px-0">
		<li class="breadcrumb-item small"><a href="'.$url.'index.php" class="text-dark">Home</a></li>
		<li class="breadcrumb-item small active" aria-current="page">'.$h1.'</li>
	</ol>
</nav>
';
$breadcrumbSub = '
<nav aria-label="breadcrumb">
	<ol class="breadcrumb bg-transparent my-2 px-0">
		<li class="breadcrumb-item small"><a href="'.$url.'index.php" class="text-dark">Home</a></li>
		<li class="breadcrumb-item small"><a href="'.$url.'1-apagar-breadcrumb.php">Mídia</a></li>
		<li class="breadcrumb-item small active" aria-current="page">'.$h1.'</li>
	</ol>
</nav>
';

$breadcrumbEstilo = '
<div class="breadcrumb py-5 bg-primary rounded-0">
	<div class="container d-flex justify-content-between">
		<h1 class="mt-2">'.$h1.'</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item small"><a href="'.$url.'index.php" class="text-dark">Home</a></li>
				<li class="breadcrumb-item small active" aria-current="page">'.$h1.'</li>
			</ol>
		</nav>
	</div>
</div>
';
$breadcrumbEstiloSub = '
<div class="breadcrumb py-5 bg-primary rounded-0">
	<div class="container d-flex justify-content-between">
		<h1 class="mt-2">'.$h1.'</h1>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item small"><a href="'.$url.'index.php" class="text-dark">Home</a></li>
				<li class="breadcrumb-item small"><a href="'.$url.'1-apagar-breadcrumb.php" class="text-dark">Breadcrumb Estilo
</a></li>
				<li class="breadcrumb-item small active" aria-current="page">'.$h1.'</li>
			</ol>
		</nav>
	</div>
</div>
';
