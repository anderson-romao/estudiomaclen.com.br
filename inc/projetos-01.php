<div class="jumbotron jumbotron-fluid">
  <div class="container">
    
    <div class="row pos-absolute mb-4">
      <div class="col-12 col-md-6"></div>
      <div class="col-12 col-md-6 text-center text-md-left">
        <h2 class="text-uppercase font-weight-bold">Projetos realizados</h2>
        <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis sapiente fugiat deserunt possimus dolorem deleniti aliquam! Fuga quam, provident numquam?</p>
        <a href="<?=$url?>projetos-relizados" class="btn bg-dark text-white" title="Projetos 3D">conheça</a>
      </div>
    </div>
    
    <div class="row mb-2">
      <div class="col-12 col-md-6 px-1 img-object-fit">
        <img src="<?=$url?>imagens/projetos-realizados/projeto-01.jpg" class="w-100" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" height="200">
      </div>
      <div class="col-12 col-md-6 px-1"></div>
    </div>
    
    <div class="row">
      <div class="col-12 col-md-6">
        
        <div class="row">
          <div class="col-12 col-md-6 px-1">
            <div class="row">
              <div class="col-12 mb-2 img-object-fit">
                <img src="<?=$url?>imagens/projetos-realizados/projeto-02.jpg" class="w-100" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" height="200">
              </div>
              <div class="col-12 mb-2 img-object-fit">
                <img src="<?=$url?>imagens/projetos-realizados/projeto-03.jpg" class="w-100" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" height="200">
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6 mb-2 px-1 img-object-fit">
            <img src="<?=$url?>imagens/projetos-realizados/projeto-04.jpg" class="w-100 h-100" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
          </div>
        </div>
        
      </div>
      
      <div class="col-12 col-md-6 px-2">
        <div class="row">
          <div class="col-12 col-md-6 mb-2 img-object-fit">
            <img src="<?=$url?>imagens/projetos-realizados/projeto-05.jpg" class="w-100" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" height="200">
          </div>
          <div class="col-12 col-md-6"></div>
        </div>
        <div class="row">
          <div class="col-12 img-object-fit">
            <img src="<?=$url?>imagens/projetos-realizados/projeto-06.jpg" class="w-100" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" height="200">
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>