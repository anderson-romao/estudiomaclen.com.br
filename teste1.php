<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php b4st_navbar_before();?>

<div class="topo bg-secondary py-1 d-none d-md-block">
  <div class="container">
    <div class="row">
      <div class="col text-left">
        <p class="m-0"><?= $rua . " - " . $bairro . " - " . $cidade . "-" . $UF ?></p>
      </div>
      <div class="col text-right">
        <a href="mailto:<?=$emailContato?>" target="_blank" rel="nofollow" title="Entre em contato" class="mr-2 text-dark"><i class="fas fa-envelope"></i></a>
        <a href="tel:<?=$ddd.$fone?>" rel="nofollow" title="Clique e ligue" class="text-dark"><i class="fas fa-phone"></i> <?=$ddd?> <strong><?=$fone?></strong></a>
      </div>
    </div>
  </div>
</div>
<nav id="navbar" class="navbar sticky-top navbar-expand-md navbar-dark bg-dark">
  <div class="container">

    <?php b4st_navbar_brand();?>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown" aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarDropdown">
      <ul class="navbar-nav py-2">
        <li class="nav-item mx-2"><a href="../" class="nav-link d-md-inline-block">Home</a></li>
        <li class="nav-item mx-2"><a href="../empresa" class="nav-link d-md-inline-block">Empresa</a></li>

        <li class="nav-item dropdown">
          <!-- a classe "dropdown-toggle" adiciona um icone dropdown -->
          <a href="<?=$url?>#" class="nav-link d-md-inline-block" data-toggle="dropdown">Exemplos</a>
          <ul class="dropdown-menu dropdown-menu-estilo">
            <li><a href="../1-apagar-menu" class="dropdown-item">Menus Modelos</a></li>
            <li><a href="../1-apagar-footer" class="dropdown-item">Footer Modelos</a></li>
            <li><a href="../1-apagar-banner-tela-completa" class="dropdown-item">Banner Tela Completa</a></li>
          </ul>
        </li>
        <li class="nav-item mx-2"><a href="../contato" class="nav-link d-md-inline-block">Contato</a></li>

        <li class="nav-item mx-2"><a href="../blog" class="nav-link d-md-inline-block">Blog</a></li>
      </ul>
      <?php
        // wp_nav_menu( array(
        //   'theme_location'  => 'navbar',
        //   'container'       => false,
        //   'menu_class'      => '',
        //   'fallback_cb'     => '__return_false',
        //   'items_wrap'      => '<ul id="%1$s" class="navbar-nav py-2 %2$s">%3$s</ul>',
        //   'depth'           => 2,
        //   'walker'          => new b4st_walker_nav_menu()
        // ) );
      ?>

      <?include('../inc/redes-sociais.php');?>
      
      <!-- Descomentar caso precise fazer buscas no blog -->
      <!-- <?php #b4st_navbar_search();?> --> 
    </div>

  </div>
</nav>

<?php b4st_navbar_after();?>