<?
$h1         = 'Estúdio Maclen';
$title      = 'Home';
$desc       = 'XXXXXXXXXXXXX';
$key        = 'decoração interiores, design de interiores, consultoria de design, decoracao de interiores barata, decoração de interiores jogos';
$var        = 'Home';
include('inc/head.php');
?>

</head>

  <body>
    <?php include('inc/header.php')?>
    
    <!-- Carousel -->
    <?include('inc/carousel-home.php');?>

    <!-- SESSÕES -->
    <main>

      <!-- Sobre a empresa -->
      <section>
        <div class="jumbotron jumbotron-fluid px-4 px-md-5 my-0">
          <div class="container">
            <img src="<?=$url?>imagens/cadeira-empresa.jpg" class="mx-auto d-block float-none float-sm-left mr-md-3 mb-3" alt="<?=$h1?>" title="<?=$h1?>" width="200">

            <div class="row text-md-left text-center border border-secondary border-bold p-0 p-md-5 mx-md-3">
              <div class="col-12 col-md-4">
                <h1 class="display-4"><?=$nomeSite?></h1>
              </div>
              <div class="col-12 col-md-8 px-4 border-xs-none border-left border-secondary">
                <h2 class="h3 my-4 mt-md-0 mb-md-2"><?=$slogan?></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat eum saepe maxime fugiat. Sit quo quia, tempora ex incidunt tenetur alias placeat obcaecati ab magnam, similique suscipit, doloremque reprehenderit dolores?</p>
              </div>
            </div>

          </div>
        </div>
      </section>

      <!-- Serviços -->
      <section>
        <div class="jumbotron jumbotron-fluid my-0">
          <div class="float-left bg-warning border py-3 px-5 box-yellow text-uppercase">Serviços</div>
          <div class="float-right bg-warning border py-3 px-5 box-yellow"><span class="fas fa-chevron-left fa-1x"></span><span class="fas fa-chevron-right fa-1x"></span></div>
          <hr class="border-secondary">

          <div class="container my-5">
            <div class="card-deck">
          
              <div class="card mb-4 mx-2 shadow">
                <img src="<?=$url?>imagens/projetos-3d-servicos.jpg" alt="Projetos 3D" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Projetos 3D</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit, ullam?</p>
                </div>
                <div class="card-footer">
                  <div class="btn-group">
                    <a href="<?=$url?>projetos-3d" title="Projetos 3D" class="btn bg-secondary text-white">Saiba mais</a>
                    <a href="<?=$url?>projetos-3d" title="Projetos 3D" class="btn btn-warning font-weight-bold"><span class="fas fa-arrow-right fa-1x"></span></a>
                  </div>
                </div>
              </div>
          
              <div class="card mb-4 mx-2 shadow">
                <img src="<?=$url?>imagens/desenv-projeto.jpg" alt="Desenvolvimento de projetos" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Desenvolvimento de projetos</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, deleniti.</p>
                </div>
                <div class="card-footer">
                  <div class="btn-group">
                    <a href="<?=$url?>desenvolvimento-de-projetos" title="Desenvolvimento de projetos" class="btn bg-secondary text-white">Saiba mais</a>
                    <a href="<?=$url?>desenvolvimento-de-projetos" title="Desenvolvimento de projetos" class="btn btn-warning font-weight-bold"><span class="fas fa-arrow-right fa-1x"></span></a>
                  </div>
                </div>
              </div>
          
              <div class="card mb-4 mx-2 shadow">
                <img src="<?=$url?>imagens/consultoria.jpg" alt="Consultoria" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Consultoria</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, adipisci.</p>
                </div>
                <div class="card-footer">
                  <div class="btn-group ">
                    <a href="<?=$url?>consultoria" title="Consultoria" class="btn bg-secondary text-white">Saiba mais</a>
                    <a href="<?=$url?>consultoria" title="Consultoria" class="btn btn-warning font-weight-bold"><span class="fas fa-arrow-right fa-1x"></span></a>
                  </div>
                </div>
              </div>
          
              <div class="card mb-4 mx-2 shadow">
                <img src="<?=$url?>imagens/acessorios.jpg" alt="Loja virtual" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Loja virtual</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam, laboriosam.</p>
                </div>
                <div class="card-footer">
                  <div class="btn-group">
                    <a href="https://lojaintegrada.com.br/" target="_blank" rel="nofollow" title="Loja virtual" class="btn bg-secondary text-white">Saiba mais</a>
                    <a href="https://lojaintegrada.com.br/" target="_blank" rel="nofollow" title="Loja virtual" class="btn btn-warning font-weight-bold"><span class="fas fa-arrow-right fa-1x"></span></a>
                  </div>
                </div>
              </div>
          
            </div>
          </div>
        </div>
      </section>

      <!-- Projetos 3D -->
      <section>
        <div class="banner-02 jumbotron jumbotron-fluid mb-0">
          <div class="h-100">
            <div class="container h-100 d-flex">
              <div class="my-auto mx-auto text-center">
                <h2 class="display-2 text-uppercase font-weight-bold">Projetos 3D</h2>
                <p class="font-weight-light h5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum quaerat fugit sit praesentium eos totam corporis nihil</p>
                <br>
                <p class="font-weight-light h5">consequuntur tenetur fugiat explicabo fuga nostrum qui quibusdam provident, minima, error odit rem.</p>
                <a href="<?=$url?>projetos-3d" class="btn btn-outline-light btn-lg text-uppercase mt-5 px-5" title="Projetos 3D">conheça</a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Projetos realizados -->
      <section>
        <?#include('inc/projetos-01.php');?>
        <?include('inc/projetos-02.php');?>
      </section>

      <!-- Segurança no serviço -->
      <section>
        <div class="container py-2">
          <h2 class="h1 font-weight-bold text-center text-md-left">Segurança no serviço</h2>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, eos praesentium necessitatibus similique, non dolor.

          <div class="row my-5">
            <div class="col-12 col-sm-3 col-md-6 col-lg-3 text-center">
              <div class="c100 p80 big blue2 mx-auto float-none">
                <span class="text-dark">80%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus placeat doloremque odit aliquid, dicta dolorem minima id dolorum distinctio et.</p>
            </div>
            
            <div class="col-12 col-sm-3 col-md-6 col-lg-3 text-center">
              <div class="c100 p40 big blue2 mx-auto float-none">
                <span class="text-dark">40%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus placeat doloremque odit aliquid, dicta dolorem minima id dolorum distinctio et.</p>
            </div>
            
            <div class="col-12 col-sm-3 col-md-6 col-lg-3 text-center">
              <div class="c100 p30 big blue2 mx-auto float-none">
                <span class="text-dark">30%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus placeat doloremque odit aliquid, dicta dolorem minima id dolorum distinctio et.</p>
            </div>
            
            <div class="col-12 col-sm-3 col-md-6 col-lg-3 text-center">
              <div class="c100 p70 big blue2 mx-auto float-none">
                <span class="text-dark">70%</span>
                <div class="slice">
                  <div class="bar"></div>
                  <div class="fill"></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus placeat doloremque odit aliquid, dicta dolorem minima id dolorum distinctio et.</p>
            </div>
          </div>

        </div>
      </section>

      <!-- Depoimentos -->
      <section>
        <?include('inc/depoimentos.php');?>
      </section>
    
      <!-- Loja virtual -->
      <section>
        <?include('inc/carousel-loja-virtual.php');?>
      </section>
          
    </main>

    <?php include('inc/footer.php') ?>

    <!-- Carousel depoimentos e loja virtual/ Script no arquivo funcoes.js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

  </body>
</html>