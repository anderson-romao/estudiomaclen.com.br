<?
$h1         = 'Menus';
$title      = 'Menus';
$desc       = 'Menus';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Menus';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>
	
	<hr class="cinza">

	<!-- Navbar centralizado -->
	<div class="navbar-expand-lg navbar-dark bg-dark">
		<div class="container text-center">
			<a href="<?=$url?>" title="ESCREVA_AQUI" class="navbar-brand">
				<img src="<?=$url?>imagens/logo.png" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" width="30" height="30" class="d-inline-block align-top">
				Layout Base Padrão
			</a>
		</div>
	</div>
	<nav class="navbar navbar-expand-sm navbar-dark bg-dark" data-toggle="affix">
	    <div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuCentro" aria-expanded="false" aria-label="Toggle navigation">
	            <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="collapse navbar-collapse text-center" id="menuCentro">
	            <ul class="navbar-nav">
	                <?include('inc/menu.php');?>
	            </ul>
	        </div>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 1 - Navbar com logo a esquerda, links ao centro e descrição a direita -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	    <div class="d-flex w-50 order-0">
	        <a class="navbar-brand mr-1" href="#">
				<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
				<?=$nomeSite?>
	        </a>
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
	            <span class="navbar-toggler-icon"></span>
	        </button>
	    </div>
	    <div class="navbar-collapse collapse justify-content-center order-2" id="collapsingNavbar">
	        <ul class="navbar-nav">
	            <?include('inc/menu.php');?>
	        </ul>
	    </div>
	    <span class="navbar-text small text-truncate mt-1 w-50 text-right order-1 order-md-last">always show</span>
	</nav>

	<hr class="cinza">

	<!-- 2 - Navbar com logo a esquerda, links ao centro e links a direita -->
	<nav class="navbar navbar-dark navbar-expand-md bg-dark justify-content-center">
	    <a href="#" class="navbar-brand mr-0">
	    	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
			<?=$nomeSite?>
	    </a> 
	    <button class="navbar-toggler ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
	        <ul class="navbar-nav mx-auto text-center">
	            <?include('inc/menu.php');?>
	        </ul>
	        <ul class="nav navbar-nav flex-row justify-content-center flex-nowrap">
	            <?include('inc/redes-sociais.php');?>
	        </ul>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 3 - Navbar com logo ao centro, links a esquerda e direita -->
	<nav class="navbar navbar-dark navbar-expand-md bg-dark justify-content-between">
	    <div class="container-fluid">
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-nav">
	            <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="navbar-collapse collapse dual-nav w-50 order-1 order-md-0">
	            <ul class="navbar-nav">
	                <?include('inc/menu.php');?>
	            </ul>
	        </div>
	        <a href="#" class="navbar-brand mx-auto d-block text-center order-0 order-md-1 w-25">
	        	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
				<?=$nomeSite?>
	        </a>
	        <div class="navbar-collapse collapse dual-nav w-50 order-2">
	            <ul class="nav navbar-nav ml-auto">
	                <?include('inc/redes-sociais.php');?>
	            </ul>
	        </div>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 4 - Navbar (normal) com logo e links a esqueda, com toggler a direita. Os links encolhem no mobile -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	    <a href="#" class="navbar-brand">
	    	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
			<?=$nomeSite?>
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar4">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="navbar-collapse collapse" id="navbar4">
	        <ul class="navbar-nav">
	            <?include('inc/menu.php');?>
	        </ul>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 5 - Navbar com logo e links a esquerda, com formulário de pesquisa de largura total a direita -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	    <a href="#" class="navbar-brand">
			<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
			<?=$nomeSite?>
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar5">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="navbar-collapse collapse justify-content-stretch" id="navbar5">
	        <ul class="navbar-nav">
	            <?include('inc/menu.php');?>
	        </ul>
	        <form class="ml-3 my-auto d-inline w-100">
	            <div class="input-group">
	              <input type="text" class="form-control border border-secondary" placeholder="Escreva aqui"> 
	              <div class="input-group-append">
	                <button class="btn btn-outline-info" type="button">Pesquisar</button>
	              </div>
	            </div>
	        </form>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 6 - Navbar com logo e link a esqueda, e com links a direita -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	    <a href="#" class="navbar-brand">
	    	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
			<?=$nomeSite?>
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar6">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="navbar-collapse collapse justify-content-stretch" id="navbar6">
	        <ul class="navbar-nav">
	            <li class="nav-item active">
	                <a class="nav-link" href="#">Link <span class="sr-only">Home</span></a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" href="#">Link</a>
	            </li>
	        </ul>
	        <ul class="navbar-nav ml-auto">
	            <?include('inc/menu.php');?>
	        </ul>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 7 - Navbar com logo a esquera e links a direita -->
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	    <a href="#" class="navbar-brand">
	    	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
			<?=$nomeSite?>
	    </a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar7">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="navbar-collapse collapse justify-content-stretch" id="navbar7">
	        <ul class="navbar-nav ml-auto">
	            <?include('inc/menu.php');?>
	        </ul>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 8 - Navbar com link ao centro e links a direita -->
	<style>
		.abs-center-x {
	    position: absolute;
	    left: 50%;
	    transform: translateX(-50%);
	}
	</style>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar8">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <span class="navbar-text">&nbsp;</span>
	    <div class="navbar-collapse collapse" id="navbar8">
	        <ul class="navbar-nav abs-center-x">
	            <li class="nav-item">
	                <a class="nav-link" href="#">Center</a>
	            </li>
	        </ul>
	        <ul class="navbar-nav ml-auto">
	            <?include('inc/menu.php');?>
	        </ul>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 9 - Navbar dentro de um container com links a direita -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <div class="container">
	        <a href="#" class="navbar-brand">
	        	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
				<?=$nomeSite?>
	        </a>
	        <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbar9">
	            <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="navbar-collapse collapse" id="navbar9">
	            <ul class="navbar-nav ml-auto">
	                <?include('inc/menu.php');?>
	            </ul>
	        </div>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 10 - Navbar justificado com link centralizados em largura total -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    <div class="container">
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar10">
	            <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="navbar-collapse collapse" id="navbar10">
	            <ul class="navbar-nav nav-fill w-100">
	                <?include('inc/menu.php');?>
	            </ul>
	        </div>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 11 - Navbar com logo e links centralizados -->
	<nav class="navbar navbar-expand-sm navbar-dark bg-dark" data-toggle="affix">
	    <div class="mx-auto d-sm-flex d-block flex-sm-nowrap">
	        <a class="navbar-brand" href="#">Brand</a>
	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample11" aria-expanded="false" aria-label="Toggle navigation">
	            <span class="navbar-toggler-icon"></span>
	        </button>
	        <div class="collapse navbar-collapse text-center" id="navbarsExample11">
	            <ul class="navbar-nav">
	                <?include('inc/menu.php');?>
	            </ul>
	        </div>
	    </div>
	</nav>

	<hr class="cinza">

	<!-- 12 - Navbar com logo a esquerda e botão e links a direita -->
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-end">
	    <a class="navbar-brand" href="#">
	    	<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
			<?=$nomeSite?>
	    </a>
	    <button class="btn btn-dark ml-auto mr-2">Botão</button>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
	        <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse flex-grow-0" id="navbarSupportedContent">
	        <ul class="navbar-nav text-right">
	            <li class="nav-item active">
	                <a class="nav-link" href="#">Link</a>
	            </li>
	            <li class="nav-item active">
	                <a class="nav-link" href="#">Link</a>
	            </li>
	        </ul>
	    </div>
	</nav>
	
	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h3 class="my-3">Modelos de menus</h3>

				<p>Modelos de menus que melhor se adapatam ao projeto</p>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>