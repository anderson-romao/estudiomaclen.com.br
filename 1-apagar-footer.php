<?
$h1         = 'Footer Modelos';
$title      = 'Footer Modelos';
$desc       = 'Footer Modelos';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Footer Modelos';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h3 class="my-3">Alguns modelos de footers</h3>

				<p>Escolhe a estrutura que melhor se enquada no projeto. Copiar o códfico fonte e inserir no projeto.</p>

			</article>
		</section>	
	</main>

	<footer class="container-fluid bg-dark pt-4">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md text-center">
					<a href="<?=$url?>" title="ESCREVA_AQUI">
						<img src="imagens/logo.png" alt="ESCREVA_AQUI" class="rounded-circle" width="100" height="100">
					</a>
				</div>
				<div class="col-6 col-md">
					<h5 class="text-secondary">Menu 1</h5>
					<ul class="list-unstyled text-small">
						<li><a href="#" class="text-secondary">Item 1</a></li>
						<li><a href="#" class="text-secondary">Item 2</a></li>
						<li><a href="#" class="text-secondary">Item 3</a></li>
						<li><a href="#" class="text-secondary">Item 4</a></li>
					</ul>
				</div>
				<div class="col-6 col-md">
					<h5 class="text-secondary">Menu 2</h5>
					<ul class="list-unstyled text-small">
						<li><a href="#" class="text-muted">Item 1</a></li>
						<li><a href="#" class="text-muted">Item 2</a></li>
						<li><a href="#" class="text-muted">Item 3</a></li>
					</ul>
				</div>
				<div class="col-6 col-md">
					<h5 class="text-secondary">Menu 3</h5>
					<ul class="list-unstyled text-small">
						<li><a href="#" class="text-secondary">Item 1</a></li>
						<li><a href="#" class="text-secondary">Item 2</a></li>
						<li><a href="#" class="text-secondary">Item 3</a></li>
						<li><a href="#" class="text-secondary">Item 4</a></li>
					</ul>
				</div>
				<div class="col-6 col-md">
					<h5 class="text-secondary">Menu 4</h5>
					<ul class="list-unstyled text-small">
						<li><a href="#" class="text-secondary">Item 2</a></li>
						<li><a href="#" class="text-secondary">Item 3</a></li>
						<li><a href="#" class="text-secondary">Item 4</a></li>
					</ul>
				</div>
				<div class="col-12 col-md">
					<?include('inc/redes-sociais.php')?>
				</div>
			</div>
		</div>
		<hr>
		<div class="container text-white-50">
			<div class="row">
				<div class="col">
					<p class="text-left small">Desenvolvido por: <?=$creditos?></p>
				</div>
				<div class="col">
					<ul class="list-inline text-right">
						<li class="list-inline-item"><a class="text-white-50 small" href="#">Link 1</a></li>
						<li class="list-inline-item"><a class="text-white-50 small" href="#">Link 2</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<hr>

	<footer class="container-fluid bg-dark pt-4">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-3 text-center">
					<a href="<?=$url?>" title="ESCREVA_AQUI">
						<img src="imagens/logo.png" alt="ESCREVA_AQUI" class="rounded-circle" width="100" height="100">
					</a>
				</div>				

				<div class="col-12 col-md-6">
					<ul class="nav justify-content-center">
						<li class="nav-item">
							<a href="<?=$url?>" title="ESCREVA_AQUI" class="nav-link text-secondary">Link 1</a>
						</li>
						<li class="nav-item">
							<a href="<?=$url?>" title="ESCREVA_AQUI" class="nav-link text-secondary">Link 2</a>
						</li>
						<li class="nav-item">
							<a href="<?=$url?>" title="ESCREVA_AQUI" class="nav-link text-secondary">Link 3</a>
						</li>
						<li class="nav-item">
							<a href="<?=$url?>" title="ESCREVA_AQUI" class="nav-link text-secondary">Link 4</a>
						</li>
						<li class="nav-item">
							<a href="<?=$url?>" title="ESCREVA_AQUI" class="nav-link text-secondary">Link 5</a>
						</li>
					</ul>
				</div>
				<div class="col-12 col-md-3">
					<?include('inc/redes-sociais.php') ?>
				</div>
			</div>
		</div>

		<hr>

		<div class="container text-white-50">
			<div class="row">
				<div class="col">
					<p class="text-left small">Desenvolvido por: <?=$creditos?></p>
				</div>
				<div class="col">
					<ul class="list-inline text-right">
						<li class="list-inline-item"><a class="text-white-50 small" href="#">Link 1</a></li>
						<li class="list-inline-item"><a class="text-white-50 small" href="#">Link 2</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<hr>

	<?include('inc/footer.php') ?>

</body>