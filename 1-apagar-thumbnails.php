<?
$h1         = 'Thumbnails';
$title      = 'Thumbnails';
$desc       = 'Thumbnails';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Thumbnails';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>

				<h2 class="my-4"><?=$h1?> com loop em for e fancybox</h2>

				<style>
					ul.img-width li img{
					    width: 200px;
					    height: auto;
					}
				</style>

				<ul class="list-inline img-width">
					<?php
						$pasta='time';
						$img='integrante';
						$nimg='9';
						for ($i = 1; $i <= $nimg; $i++) {
						$i < 10 ? $zero = 0 : $zero = "";
						echo'
						<a href="'.$url.'imagens/'.$pasta.'/'.$img.'-'.$zero.$i.'.jpg" title="'.$img.'" data-toggle="lightbox" data-gallery="grupo1">
							<li class="list-inline-item pb-2">
								<img src="'.$url.'imagens/'.$pasta.'/'.$img.'-'.$zero.$i.'.jpg" alt="'.$img.'" title="'.$img.'" class="img-fluid mr-1">
							</li>
						</a>

						<div class="card-deck">
							<div class="card">
								<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
								<div class="card-body">
									<h5 class="card-title">Título</h5>
								</div>
							</div>
						</div>

						';
						}
					?>
				</ul>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

	<script>
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: false,
				wrapping: false,
			});
		});
	</script>

</body>