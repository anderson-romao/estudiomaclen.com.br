<?
$h1         = 'Imagens';
$title      = 'Imagens';
$desc       = 'Imagens';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Imagens';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h2 class="my-4"><?=$h1?> alinhadas com "li"</h2>

				<ul class="list-inline">
					<li class="list-inline-item pb-2">
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
					</li>
					<li class="list-inline-item pb-2">
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
					</li>
					<li class="list-inline-item pb-2">
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
					</li>
					<li class="list-inline-item pb-2">
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
					</li>
					<li class="list-inline-item pb-2">
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
					</li>
				</ul>

				<hr class="cinza my-5">

				<h2 class="my-4"><?=$h1?> com loop em for e fancybox</h2>

				<style>
					ul.img-width li img{
					    width: 200px;
					    height: auto;
					}
				</style>

				<ul class="list-inline img-width">
					<?php
						$pasta='time';
						$img='integrante';
						$nimg='9';
						for ($i = 1; $i <= $nimg; $i++) {
						$i < 10 ? $zero = 0 : $zero = "";
						echo'
						<a href="'.$url.'imagens/'.$pasta.'/'.$img.'-'.$zero.$i.'.jpg" title="'.$img.'" data-toggle="lightbox" data-gallery="grupo1">
							<li class="list-inline-item pb-2">
								<img src="'.$url.'imagens/'.$pasta.'/'.$img.'-'.$zero.$i.'.jpg" alt="'.$img.'" title="'.$img.'" class="img-fluid mr-1">
							</li>
						</a>
						';
						}
					?>
				</ul>

				<hr class="cinza">

				<h2 class="my-4"><?=$h1?> com placeholder, loop em for e fancybox</h2>
				<ul class="list-inline">
					<?php
						$pasta='time';
						$img='integrante';
						$nimg='10';
						for ($i = 1; $i <= $nimg; $i++) {
						$i < 10 ? $zero = 0 : $zero = "";
						echo'
						<a href="http://via.placeholder.com/100" title="'.$img.'" data-toggle="lightbox">
							<li class="list-inline-item pb-2">
								<img src="http://via.placeholder.com/100" alt="'.$img.'" title="'.$img.'" class="img-fluid mr-1">
							</li>
						</a>
						';
						}
					?>
				</ul>


				<hr class="cinza my-5">

				<h2>Alinhamento de imagens</h2>
				
				<div class="clearfix">
					<figure class="figure float-left">
						<figcaption class="figure-caption">float-left</figcaption>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="float-left">
					</figure>
					<figure class="figure float-right">
						<figcaption class="figure-caption text-right">float-right</figcaption>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="float-right">
					</figure>
				</div>

				<div class="clearfix">
					<figure class="figure mx-auto d-block">
						<figcaption class="figure-caption text-center">mx-auto d-block</figcaption>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="mx-auto d-block">
					</figure>
				</div>

				<hr class="cinza my-5">

				<h2>Formas de imagens</h2>

				<ul class="list-inline text-center">
					<li class="list-inline-item mx-4">
						<p class="m-0">imagem normal</p>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">
					</li>
					<li class="list-inline-item mx-4">
						<p class="m-0">class="rounded"</p>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="rounded">
					</li>
					<li class="list-inline-item mx-4">
						<p class="m-0">class="rounded-circle"</p>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="rounded-circle">
					</li>
					<li class="list-inline-item mx-4">
						<p class="m-0">class="img-thumbnail"</p>
						<img src="http://via.placeholder.com/200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-thumbnail">
					</li>
				</ul>

				<hr class="cinza my-5">

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

	<script>
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: false,
				wrapping: false,
			});
		});
	</script>

</body>