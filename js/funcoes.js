// Menu transparente com scroll
$(function(){
  $(window).scroll(function(){
    if ($(this).scrollTop() > 20){
      $(".cor-nav").addClass("bg-dark");
    }else{
      $(".cor-nav").removeClass("bg-dark");
    }
  });
});

// Carousel Depoimentos
$(document).ready(function(){
	$('.depoimentos').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000,
		arrows: true,
		dots: false,
		prevArrow:'<i class="fas fa-angle-left fa-2x text-secondary btn-slick-carousel-left"></i>',
		nextArrow:'<i class="fas fa-angle-right fa-2x text-secondary btn-slick-carousel-right"></i>',
		pauseOnHover: false,
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 1
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1
			}
		}]
	});
});

// Carousel Loja Virtual
$(document).ready(function(){
	$('.customer-cards').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: true,
		dots: false,
		prevArrow:'<i class="fas fa-angle-left fa-2x text-secondary btn-slick-carousel-left"></i>',
		nextArrow:'<i class="fas fa-angle-right fa-2x text-secondary btn-slick-carousel-right"></i>',
		pauseOnHover: false,
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 1
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1
			}
		}]
	});
});

// Exemplo de JavaScript inicial para desativar envios de formulário, se houver campos inválidos.
(function() {
	'use strict';
	window.addEventListener('load', function() {
	    // Pega todos os formulários que nós queremos aplicar estilos de validação Bootstrap personalizados.
	    var forms = document.getElementsByClassName('needs-validation');
	    // Faz um loop neles e evita o envio
	    var validation = Array.prototype.filter.call(forms, function(form) {
	    	form.addEventListener('submit', function(event) {
	    		if (form.checkValidity() === false) {
	    			event.preventDefault();
	    			event.stopPropagation();
	    		}
	    		form.classList.add('was-validated');
	    	}, false);
	    });
	}, false);
})();

//Back to top button
var btn = $('#buttonTop');
//aparecer botão depois de descer 150px
$(window).scroll(function() {
  if ($(window).scrollTop() > 150) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});
//função
btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '700', 'swing');
  return false;
});