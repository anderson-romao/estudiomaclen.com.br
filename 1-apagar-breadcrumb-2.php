<?
$h1         = 'Breadcrumb 2';
$title      = 'Breadcrumb 2';
$desc       = 'Breadcrumb 2';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Breadcrumb 2';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<?=$breadcrumbEstiloSub?>
		<section class="container my-5">
			<article>
				
				<h3><?=$h1?></h3>

				<img src="http://via.placeholder.com/200" class="mx-auto d-block float-none float-sm-left pr-3 mb-4" alt="ESCREVA_AQUI" title="ESCREVA_AQUI">

				<p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>