<?
$h1         = 'Modelo One Page';
$title      = 'Modelo One Page';
$desc       = 'Modelo One Page';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Modelo One Page';
include('inc/head.php');
?>

</head>

<body>

	<?include('inc/header.php') ?>

	<!-- 4 - Navbar (normal) com logo e links a esqueda, com toggler a direita. Os links encolhem no mobile -->
	
	<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary">
	    <div class="container">
	    	<a href="#" class="navbar-brand">
	    		<img src="<?=$url?>imagens/logo.png" alt="<?=$nomeSite?>" width="30" class="d-inline-block align-top">
	    				<?=$nomeSite?>
	    	</a>
	    	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar4">
	    	    <span class="navbar-toggler-icon"></span>
	    	</button>
	    	<div class="navbar-collapse collapse" id="navbar4">
	    	    <ul class="navbar-nav">
	    	        <li class="nav-item mx-2"><a href="#section1" class="nav-link desliza">Artigo 1</a></li>
	    	        <li class="nav-item mx-2"><a href="#section2" class="nav-link desliza">Artigo 2</a></li>
	    	        <li class="nav-item mx-2"><a href="#section3" class="nav-link desliza">Artigo 3</a></li>
	    	        <li class="nav-item mx-2"><a href="#section4" class="nav-link desliza">Artigo 4</a></li>					
	    	    </ul>
	    	</div>
	    </div>
	</nav>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
		</section>

		<section id="section1">
			<article class="container">
				<h2>Artigo 1</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus beatae, alias neque, perferendis impedit saepe. Minus nostrum, ex! Natus eveniet, optio, vitae quo, suscipit itaque ab ut accusamus explicabo quibusdam nesciunt ullam tempora numquam possimus minima voluptate dignissimos exercitationem? In, itaque illo, rem explicabo cum odit architecto! Sunt possimus a praesentium, delectus debitis. Asperiores ipsa ullam nemo aperiam iusto dignissimos tempora. Possimus, ab? Odio iusto blanditiis, fuga officiis culpa ullam corporis labore non vel nam earum. Facilis et esse enim voluptas fuga vitae, alias, quasi perferendis quam. Non amet, aut velit. Iure temporibus voluptate, placeat in mollitia natus magnam quam culpa, quis, aspernatur beatae ad. Asperiores id ullam in, odio fugiat repellat, quo. Perspiciatis aut error maiores recusandae iste tempora, non dolor corporis officiis iusto, nam reiciendis. Assumenda, eos, eum.</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque natus voluptatibus, tempore officiis dolorem veniam provident, ex. Quas delectus illo accusamus culpa maiores, ad tempore voluptas eveniet, repellat eaque molestias quae tenetur commodi iure repellendus dolor, sequi aperiam non alias, ex quod incidunt quibusdam explicabo! Voluptatem ea voluptatibus odit, provident, distinctio quod culpa aliquid iusto quas aliquam doloribus ab recusandae, earum nisi sapiente cumque maxime dolorem nemo! Delectus, illum explicabo eum facere. Impedit perferendis, vel eligendi, labore, incidunt fugit ipsa architecto illo voluptatum laudantium, corrupti magnam minus ipsam! Vero, repudiandae, provident. Et adipisci placeat laboriosam repellat, vel perspiciatis corporis, asperiores ad similique, ipsam quisquam consequatur sed veritatis quia recusandae quasi. Nesciunt recusandae natus rerum aut fugit corporis obcaecati eius perspiciatis dolorum perferendis in, ad blanditiis, mollitia quae ducimus, porro aliquid enim quidem sint. Dolores nemo, beatae fugiat possimus iusto placeat aliquid est totam, quisquam eius veritatis. Eaque dolorum labore, assumenda. Dolorem, reprehenderit. Aut quod perspiciatis maxime commodi suscipit quia quas ex soluta natus dolore modi, nulla voluptates temporibus ea nam qui. Non, dolor. Magnam ab, exercitationem aspernatur accusantium harum, ad accusamus, tempore inventore rem deserunt quam amet repellat, nihil odio molestias! Consequatur nam nostrum quo debitis earum iure id officiis!</p>
			</article>
		</section>

		<section id="section2">
			<article class="container">
				<h2>Artigo 2</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis fugiat reprehenderit, accusamus numquam vero ad, suscipit nisi ea veniam assumenda rerum dolorum, voluptatem est, dignissimos. A accusamus, nam voluptas facilis omnis quia soluta, aut sequi minima aliquid quibusdam nihil alias!</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus at suscipit odio, vitae ab, cumque minus sapiente doloribus nulla illo voluptatum maiores sed laudantium autem expedita incidunt iusto similique. Illum, dignissimos, veniam! Totam ipsam fuga, maxime id beatae nam nulla consequuntur odit odio excepturi mollitia aspernatur est maiores repudiandae nihil, vel dicta ea reprehenderit autem earum nostrum cupiditate iusto sit. Ad molestiae, quae autem. Autem adipisci natus similique cumque reprehenderit veniam at nemo delectus vero repudiandae quibusdam tempora eius nihil modi, voluptatum, ab earum. Accusamus, omnis neque illum tempora tempore qui. Accusamus in, eaque, hic temporibus numquam atque voluptate molestiae itaque, ab sed dicta impedit? Atque assumenda minima, harum tempora architecto ut non libero sed autem quam quaerat magni beatae, doloribus minus, inventore error, magnam consectetur cumque officiis obcaecati ipsum dicta aliquam rerum labore. Voluptatum saepe dicta ut quod, aspernatur, maxime ad alias laudantium ipsum placeat tenetur officiis molestias eaque accusamus praesentium voluptatem nisi non obcaecati magni expedita quo amet iste? Ducimus voluptas, tempore dolorum molestias, recusandae sapiente laboriosam. Earum porro delectus, voluptate ullam odio dolorum, impedit quod rem magni quo minima expedita dicta animi architecto vero non cumque nulla inventore debitis minus ut, culpa veniam eius nam facere. Veniam quis facilis, officia totam doloremque sit, harum, maiores doloribus cupiditate libero neque. Tenetur cupiditate, est mollitia incidunt consequuntur dignissimos voluptate autem fugit eveniet ut aperiam harum assumenda, temporibus quas nam laborum vel, ducimus tempora inventore repellat. Amet facere vero veritatis commodi id repellendus unde tempora aut, fuga nobis, labore! Perferendis eius obcaecati minus beatae ullam aliquid cum ad culpa quia facere saepe assumenda optio deserunt labore ipsa quisquam, quo earum odio corporis alias voluptate excepturi. Quaerat ipsum, itaque ex nam atque repudiandae voluptatibus voluptate facere consectetur eum, ab veritatis. Necessitatibus neque voluptate ullam nihil distinctio, laborum enim. Laborum, consequatur, similique!</p>
			</article>
		</section>

		<section id="section3">
			<article class="container">
				<h2>Artigo 3</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ad pariatur sint, quos non sit tenetur quod, ratione deserunt vero quasi soluta, voluptatem doloremque facilis dolores. Facilis sapiente neque ex ut enim voluptates dolore? Explicabo tempora perferendis quia! Dolores expedita harum labore tenetur quos corporis, ex deleniti inventore eius laudantium ab nostrum veritatis debitis, aut quibusdam, quaerat. Inventore sunt eius voluptatum itaque, dolore eaque dolorem asperiores ipsam quasi nobis aut nisi optio aliquid ea, rem autem sed ut exercitationem voluptatibus dolorum voluptatem at accusamus quod. Provident aliquam, asperiores nostrum magnam odio ea itaque numquam quaerat earum quae dolore, fugiat nesciunt.</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus distinctio veniam hic magnam quibusdam tempore quaerat laborum, et commodi consectetur fugiat culpa molestias in deserunt, laboriosam magni quod veritatis. Rem aut delectus ipsam itaque repellat illum suscipit libero tenetur dolorum eaque magnam enim dicta consequatur illo, reprehenderit, possimus quia laborum adipisci, quisquam sed. Quas fugit vero cupiditate. Eos dicta nisi enim, provident perferendis adipisci ea deserunt. Omnis impedit, nemo repellendus quaerat. Neque quod eius consequuntur similique repellat accusantium quidem atque expedita est dolores, quis officiis rerum? Soluta quasi aliquam provident in nulla corporis, ipsa harum, impedit optio quam consectetur excepturi.</p>
			</article>
		</section>

		<section id="section4">
			<article class="container">
				<h2>Artigo 4</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi est accusamus perferendis, neque praesentium recusandae dignissimos vitae optio expedita. Debitis autem quam eos illum explicabo aperiam sit fugit. Sapiente dicta laborum omnis assumenda aspernatur tempore rem totam! Expedita ducimus laboriosam earum tenetur laudantium ullam temporibus, tempore ratione ipsa porro, repellendus.</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, doloribus nulla earum velit fuga quos dolorum ad eaque nam debitis aliquid voluptates minus cumque vero est, commodi aperiam impedit aliquam maiores, magni voluptas suscipit ratione illum fugiat culpa. Suscipit earum numquam odio officia nisi unde dolore aperiam quos, rerum fugiat commodi sequi harum aspernatur temporibus iste blanditiis quis exercitationem, repellendus, aliquid provident. Optio architecto quas, ducimus! Atque fugit voluptates ipsam eos repudiandae officiis earum ad, quas, excepturi iusto saepe esse autem asperiores non! Totam tempora, itaque laborum culpa officia id, repellendus dolore, dolores quo reiciendis sequi at? Fuga quisquam in voluptas reprehenderit adipisci, autem ipsam aperiam nulla sequi blanditiis doloribus, quod asperiores earum magni exercitationem fugiat similique molestias libero culpa sint, necessitatibus dignissimos. Placeat voluptatibus error nemo vitae, qui! Sed repellendus eos quo, exercitationem dolorem labore repellat dolore architecto, cumque provident officia tempora voluptate, dicta voluptatem illo quisquam et nam! Facilis atque ab, cum at quis amet dicta, expedita maxime nesciunt recusandae, doloremque soluta praesentium enim rem voluptatibus corrupti nihil harum eligendi. Molestias error labore voluptatibus itaque, reiciendis repellendus exercitationem fugiat repellat, illum nihil sed? Quo officia culpa odio rerum, non error pariatur nostrum amet adipisci quidem optio fugiat magni sapiente animi eos aliquid obcaecati! Dolore voluptates explicabo quasi sunt fuga quo laboriosam sint tempore in autem necessitatibus sapiente sed, repellendus iste! Repellendus eius obcaecati sequi saepe alias, nisi quisquam. Debitis accusamus facere temporibus iusto tenetur corporis dolorem optio et. Accusantium, repellendus odit fugit nemo laboriosam, ex! Unde debitis ratione rerum id nam similique officiis repudiandae omnis, distinctio molestiae nostrum adipisci voluptatum dignissimos asperiores quo recusandae possimus minus. Nobis officia vero est culpa autem ipsum, aperiam, neque esse omnis eum, voluptatem, sint voluptatum libero quia ex? Provident sint obcaecati, illum architecto ullam vitae, rem quas a autem veniam doloribus doloremque!</p>
			</article>
		</section>
	</main>

	<?include('inc/footer.php') ?>

	<script>
		//Transição suave entre os menus
		$(function() {
		  // Handler for .ready() called.
		  // Deslize suave
		  $(".desliza").on("click", function (e) {
		    e.preventDefault();
		    var target = this.hash;
		    var $target = $(target);
		    console.log($target.offset().top); 
		    $("html, body").stop().animate({"scrollTop": ($target.offset().top - ($("header").hasClass("fixed-top") ? $("header").height() : 70 ))}, 900, "swing", null); //se header tiver a classe fixed-top faça "$("header").height()", se não, faça "100"
		    console.log($target.offset().top);
		  });
		});
	</script>

</body>