<?
$h1         = 'Banner Tela Completa';
$title      = 'Banner Tela Completa';
$desc       = 'Banner Tela Completa';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Banner Tela Completa';
include('inc/head.php');
?>

<style>
.banner-01 {
  position: relative;
  color: white;
  background: linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)),url(imagens/slides/banner-05.jpg) no-repeat center center;
  background-size: cover;
  height: 100vh;
}

.banner-02 {
  position: relative;
  color: white;
  background: linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)),url(imagens/slides/banner-06.jpg) no-repeat center center;
  background-size: cover;
  background-attachment: fixed;
  height: 100vh;
}

.banner-03 .carousel-item{
  height: 100vh;
  min-height: 350px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>

</head>

<body>
	<?include('inc/header.php') ?>

	<div id="index" class="banner-01 jumbotron jumbotron-fluid mb-0">
    <div class="h-100">
      <div class="container h-100 d-flex">
        <div class="my-auto mx-auto texto">
          <img class="mx-auto d-block" src="imagens/logo.png" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" width="40">
          <h1 class="display-3 font-media text-center mb-0"><?=$nomeSite?></h1>
          <p class="text-center h1 sub-h2 mb-4 linha"><span><?=$slogan?></span></p>
          <div class="redes-sociais text-center">
            <a href="<?=$url?>empresa" class="btn btn-outline-light btn-lg" title="ESCREVA_AQUI">Saiba mais sobre a <?=$nomeSite?></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div id="index" class="banner-02 jumbotron jumbotron-fluid mb-0">
    <div class="h-100">
      <div class="container h-100 d-flex">
        <div class="my-auto mx-auto texto">
          <img class="mx-auto d-block" src="imagens/logo.png" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" width="40">
          <h1 class="display-3 font-media text-center mb-0"><?=$nomeSite?></h1>
          <p class="text-center h1 sub-h2 mb-4 linha"><span><?=$slogan?></span></p>
          <div class="redes-sociais text-center">
            <a href="<?=$url?>empresa" class="btn btn-outline-light btn-lg" title="ESCREVA_AQUI">Saiba mais sobre a <?=$nomeSite?></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="banner-03">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One -->
        <div class="carousel-item active" style="background-image: url('https://source.unsplash.com/LAaSoL0LrYs/1920x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h2 class="display-4">Primeiro Slide</h2>
            <p class="lead">Descrição do primeiro slide.</p>
          </div>
        </div>
        <!-- Slide Two -->
        <div class="carousel-item" style="background-image: url('https://source.unsplash.com/bF2vsubyHcQ/1920x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h2 class="display-4">Segundo Slide</h2>
            <p class="lead">Descrição do segundo slide.</p>
          </div>
        </div>
        <!-- Slide Three -->
        <div class="carousel-item" style="background-image: url('https://source.unsplash.com/szFUQoyvrxM/1920x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h2 class="display-4">Terceiro Slide</h2>
            <p class="lead">Descrição do terceiro slide.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

  <hr>

	<?include('inc/footer.php') ?>

</body>