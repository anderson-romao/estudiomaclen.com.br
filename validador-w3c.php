<?
include('inc/head.php');
?>
</head>
<body>
<ul class="sitemap">
   <? include('inc/menu-top.php');?>
</ul>
<script>
$(window).load(function(){
   var validationArray = [];
   $('.sitemap a').each(function(){
       validationArray.push($(this));
   });
   $('.sitemap').empty();
   $(validationArray).each(function(){
       $('.sitemap').append($(this));
       $(this).css({'display':'block'});
   });

   CallValidationWindow();

   function CallValidationWindow(){
    var counter = 0;
    setInterval(function(){
    window.open('http://validator.w3.org/check?uri='+validationArray[counter].attr('href'), '_blank');
    $('.sitemap a[href="' + validationArray[counter].attr('href') + '"]').remove();
    counter++;
    if(validationArray.length <= 0) clearTimeout();
    }, 1000);
  }
});

</script>
</body>