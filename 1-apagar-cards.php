<?
$h1         = 'Cards';
$title      = 'Cards';
$desc       = 'Cards';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Cards';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h2 class="my-4">Modelos de Cards</h2>

				<h3 class="my-3">Conjunto de Cards com altura e largura iguais</h3>
				<div class="card-deck">
					<div class="card">
						<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse nihil incidunt.</p>
							<p class="card-text"><small class="text-muted">Atualizado a 3 minutos atrás</small></p>
						</div>
					</div>
					<div class="card">
						<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse nihil incidunt.</p>
							<p class="card-text"><small class="text-muted">Atualizado a 3 minutos atrás</small></p>
						</div>
					</div>
					<div class="card">
						<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse nihil incidunt.</p>
							<p class="card-text"><small class="text-muted">Atualizado a 3 minutos atrás</small></p>
						</div>
					</div>
					<div class="card">
						<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse nihil incidunt.</p>
							<p class="card-text"><small class="text-muted">Atualizado a 3 minutos atrás</small></p>
						</div>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3 class="my-3">Conjunto de Cards com alinhamento de texto</h3>
				<div class="card-deck">
					<div class="card">
						<div class="card-body">
							<div class="card-title">Alinhamento esquerdo</div>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Visitar</a>
						</div>
					</div>
					<div class="card text-center">
						<div class="card-body">
							<div class="card-title">Alinhamento centralizado</div>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Visitar</a>
						</div>
					</div>
					<div class="card text-right">
						<div class="card-body">
							<div class="card-title">Alinhamento direito</div>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Visitar</a>
						</div>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3 class="my-3">Conjunto de Cards com background diferentes</h3>
				<div class="card-deck mb-3">
					<div class="card text-white bg-primary">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
					<div class="card text-white bg-secondary">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
					<div class="card text-white bg-success">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
					<div class="card text-white bg-danger">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
				</div>
				<div class="card-deck">
					<div class="card text-white bg-warning">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
					<div class="card text-white bg-info">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
					<div class="card bg-light">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
					<div class="card text-white bg-dark">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título card</h5>
							<div class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3 class="my-3">Conjunto de Cards com bordas diferentes</h3>
				<div class="card-deck mb-3">
					<div class="card border-primary">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-primary">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
					<div class="card border-secondary">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-secondary">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
					<div class="card border-success">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-success">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
					<div class="card border-danger">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-danger">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
				</div>
				<div class="card-deck">
					<div class="card border-warning">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-warning">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
					<div class="card border-info">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-info">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
					<div class="card border-light">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
					<div class="card border-dark">
						<div class="card-header">Cabeçalho</div>
						<div class="card-body text-dark">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3 class="my-3">Conjunto de Cards sem espaçamento</h3>
				<div class="card-group">
					<div class="card border-dark">
						<img src="https://via.placeholder.com/258x180" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
						<div class="card-footer">
							<small class="text-muted">Atualizados 3 minutos atrás</small>
						</div>
					</div>
					<div class="card border-dark">
						<img src="https://via.placeholder.com/258x180" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
						<div class="card-footer">
							<small class="text-muted">Atualizados 3 minutos atrás</small>
						</div>
					</div>
					<div class="card border-dark">
						<img src="https://via.placeholder.com/258x180" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
						<div class="card-body">
							<h5 class="card-title">Título</h5>
							<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
						<div class="card-footer">
							<small class="text-muted">Atualizados 3 minutos atrás</small>
						</div>
					</div>
				</div>
				
				<hr class="cinza my-5">

				<h3 class="my-3">Conjunto estilo Masonry / Pinterest</h3>
				<div class="mx-5">
					<div class="card-columns">
						<div class="card border-secondary">
							<img src="https://via.placeholder.com/246x160" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							<div class="card-body">
								<h5 class="card-title">Título</h5>
								<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							</div>
						</div>
					
						<div class="card border-secondary p-3">
							<blockquote class="blockquote mb-0 card-body">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
								<footer class="blockquote-footer">
									<small class="text-muted">
										Alguém famoso em <cite title="Título da fonte">Título da fonte</cite>
									</small>
								</footer>
							</blockquote>
						</div>
					
						<div class="card border-secondary">
							<img class="card-img-top" src="https://via.placeholder.com/246x160" alt="Imagem de capa do card">
							<div class="card-body">
								<h5 class="card-title">Título do card</h5>
								<p class="card-text">Este é um card com suporte a texto embaixo, que funciona como uma introdução a um conteúdo adicional.</p>
								<p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
							</div>
						</div>
					
						<div class="card  bg-primary text-white text-center p-3">
							<blockquote class="blockquote mb-0">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
								<footer class="blockquote-footer">
									<small>
										Alguém famoso em <cite title="Título da fonte">Título da fonte</cite>
									</small>
								</footer>
							</blockquote>
						</div>
					
						<div class="card border-secondary text-center">
							<div class="card-body">
								<h5 class="card-title">Título do card</h5>
								<p class="card-text">Este é um card com suporte a texto embaixo, que funciona como uma introdução a um conteúdo adicional.</p>
								<p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
							</div>
						</div>
					
						<div class="card border-secondary">
							<img class="card-img" src="https://via.placeholder.com/246x260" alt="Imagem do card">
						</div>
					
						<div class="card border-secondary p-3 text-right">
							<blockquote class="blockquote mb-0">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
								<footer class="blockquote-footer">
									<small class="text-muted">
										Alguém famoso em <cite title="Título da fonte">Título da fonte</cite>
									</small>
								</footer>
							</blockquote>
						</div>
					
						<div class="card border-secondary">
							<div class="card-body">
								<h5 class="card-title">Título do card</h5>
								<p class="card-text">Este é um card maior com suporte a texto embaixo, que funciona como uma introdução a um conteúdo adicional. Este card tem o conteúdo ainda maior que o primeiro, para mostrar a altura igual, em ação.</p>
								<p class="card-text"><small class="text-muted">Atualizados 3 minutos atrás</small></p>
							</div>
						</div>
					
						<div class="card border-secondary">
							<img src="https://via.placeholder.com/146x250" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							<div class="card-body">
								<h5 class="card-title">Título</h5>
								<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
							</div>
						</div>
					</div>
				</div>


				<hr class="cinza my-5">

				<h3 class="my-3">Card com capas como imagem</h3>
				<div class="card">
					<img src="https://via.placeholder.com/776x180" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
					<div class="card-body">
						<div class="card-title">Título</div>
						<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, corporis necessitatibus.</p>
						<p class="card-text"><small class="text-muted">Atualizados a 3 minutos atrás</small></p>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3 class="my-3">Card com sobreposição de imagem</h3>
				<div class="card bg-dark text-white">
					<img src="https://via.placeholder.com/781x270/767676" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img">
					<div class="card-img-overlay m-5">
						<h5 class="card-title">Título</h5>
						<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero explicabo modi sunt recusandae minima rerum ab, aliquid facere quis harum velit quod maxime dolorum sequi iusto nobis autem perferendis, inventore.</p>
						<p class="card-text">Atulizados 3 minutos atrás</p>
					</div>
				</div>

				<hr class="cinza my-5">
				
				<h3 class="my-3">Card com animação 1</h3>

				<style>
					/* ----- Animação Card ----- */
					.demo-3 {
					    position:relative;
					    width:100%;
					    height:100%;
					    overflow:hidden;
					    float:left;
					    margin-right:20px
					}
					.demo-3 figure {
					    margin:0;
					    padding:0;
					    position:relative;
					    cursor:pointer;
					    margin-left:-50px
					}
					.demo-3 figure img {
					    display:block;
					    position:relative;
					    z-index:10;
					    margin:-15px 0
					}
					.demo-3 figure figcaption {
					    display:block;
					    position:absolute;
					    z-index:5;
					    -webkit-box-sizing:border-box;
					    -moz-box-sizing:border-box;
					    box-sizing:border-box
					}
					.demo-3 figure h2 {
					    font-family:'Lato';
					    color:#fff;
					    font-size:20px;
					    text-align:left
					}
					.demo-3 figure p {
					    display:block;
					    font-family:'Lato';
					    font-size:12px;
					    line-height:18px;
					    margin:0;
					    color:#fff;
					    text-align:left
					}
					.demo-3 figure figcaption {
					    top:0;
					    left:0;
					    width:100%;
					    height:100%;
					    padding:29px 44px;
					    background-color:rgba(26,76,110,0.5);
					    text-align:center;
					    backface-visibility:hidden;
					    -webkit-transform:rotateY(-180deg);
					    -moz-transform:rotateY(-180deg);
					    transform:rotateY(-180deg);
					    -webkit-transition:all .5s;
					    -moz-transition:all .5s;
					    transition:all .5s
					}
					.demo-3 figure img {
					    backface-visibility:hidden;
					    -webkit-transition:all .5s;
					    -moz-transition:all .5s;
					    transition:all .5s
					}
					.demo-3 figure:hover img,figure.hover img {
					    -webkit-transform:rotateY(180deg);
					    -moz-transform:rotateY(180deg);
					    transform:rotateY(180deg)
					}
					.demo-3 figure:hover figcaption,figure.hover figcaption {
					    -webkit-transform:rotateY(0);
					    -moz-transform:rotateY(0);
					    transform:rotateY(0)
					}
				</style>
				
				<div class="row">
					<div class="col">
						<ul class="demo-3 mx-auto">
							<li>
								<figure>
									<img src="https://via.placeholder.com/250" class="w-100" alt=""/>
									<figcaption>
										<h2>This is a cool title!</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
									</figcaption>
								</figure>
							</li>
						</ul>
					</div>
					<div class="col">
						<ul class="demo-3 mx-auto">
							<li>
								<figure>
									<img src="https://via.placeholder.com/250" class="w-100" alt=""/>
									<figcaption>
										<h2>This is a cool title!</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
									</figcaption>
								</figure>
							</li>
						</ul>
					</div>
					<div class="col">
						<ul class="demo-3 mx-auto">
							<li>
								<figure>
									<img src="https://via.placeholder.com/250" class="w-100" alt=""/>
									<figcaption>
										<h2>This is a cool title!</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
									</figcaption>
								</figure>
							</li>
						</ul>
					</div>
					<div class="col">
						<ul class="demo-3 mx-auto">
							<li>
								<figure>
									<img src="https://via.placeholder.com/250" class="w-100" alt=""/>
									<figcaption>
										<h2>This is a cool title!</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nost.</p>
									</figcaption>
								</figure>
							</li>
						</ul>
					</div>
				</div>

				<hr class="cinza my-5">

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>