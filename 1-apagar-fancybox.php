<?
$h1         = 'FancyBox';
$title      = 'FancyBox';
$desc       = 'FancyBox';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'FancyBox';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h2 class="my-4">Imagens e videos que abrem via fancybox</h2>

				<h3>Imagens individuais</h3>
				<div class="row">
					<div class="col">
						<a href="imagens/imagem-02.jpg" title="ESCREVA_AQUI" data-fancybox data-title="Título" data-footer="Rodapé customizado">
							<img src="imagens/imagem-02.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="col">
						<a href="imagens/imagem-03.jpg" title="ESCREVA_AQUI" data-fancybox>
							<img src="imagens/imagem-03.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="col">
						<a href="imagens/imagem-04.jpg" title="ESCREVA_AQUI" data-fancybox>
							<img src="imagens/imagem-04.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="col">
						<a href="imagens/imagem-05.jpg" title="ESCREVA_AQUI" data-fancybox>
							<img src="imagens/imagem-05.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3>Grupo de imagens (com setas nas laterais das imagens)</h3>
				<div class="row">
					<div class="col">
						<a href="imagens/imagem-02.jpg" title="ESCREVA_AQUI" data-fancybox="grupo-01" class="borda2">
							<img src="imagens/imagem-02.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="col">
						<a href="imagens/imagem-03.jpg" title="ESCREVA_AQUI" data-fancybox="grupo-01" class="borda2">
							<img src="imagens/imagem-03.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="col">
						<a href="imagens/imagem-04.jpg" title="ESCREVA_AQUI" data-fancybox="grupo-01" class="borda2">
							<img src="imagens/imagem-04.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="col">
						<a href="imagens/imagem-05.jpg" title="ESCREVA_AQUI" data-fancybox="grupo-01" class="borda2">
							<img src="imagens/imagem-05.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3>Videos</h3>
				<div class="row">
					<div class="col">
						<a href="https://www.youtube.com/embed/hA6hldpSTF8" data-fancybox="grupo-02">
							<p>Vingadores - Ultimato (tamanho padrão)</p>
							<img src="http://i1.ytimg.com/vi/hA6hldpSTF8/mqdefault.jpg" class="img-fluid">
						</a>
					</div>
					
					<div class="col">
						<a href="https://www.youtube.com/embed/hA6hldpSTF8" data-fancybox="grupo-02" data-width="640">
							<p>Vingadores - Ultimato (1280 x 780)</p>
							<img src="http://i1.ytimg.com/vi/hA6hldpSTF8/mqdefault.jpg" class="img-fluid">
						</a>
					</div>
					
					<div class="col">
						<a href="https://www.youtube.com/embed/hA6hldpSTF8" data-fancybox="grupo-02" data-width="1280">
							<p>Vingadores - Ultimato (640 x 360)</p>
							<img src="http://i1.ytimg.com/vi/hA6hldpSTF8/mqdefault.jpg" class="img-fluid">
						</a>
					</div>
				</div>

				<hr class="cinza my-5">

				<h3>Páginas carregadas via iframe</h3>

				
				<a href="https://getbootstrap.com.br/" data-width="900" class="fullscreen">
					Bootstrap 4
				</a>
				<br>
				<a href="https://legiaodosherois.uol.com.br/" data-width="1200" class="desktop">
					Legião de Heróis
				</a>
				<br>
				<a href="https://legiaodosherois.uol.com.br/" data-width="400" class="mobile">
					Legião de Heróis (mobile)
				</a>
				<br>




				


				<a data-fancybox data-type="iframe" data-src="https://olhardigital.com.br/" href="javascript:;" class="fullscreen">
					Webpage full
				</a><br>

				<a data-fancybox data-type="iframe" data-src="https://getbootstrap.com.br/" href="javascript:;" class="desktop">
					Webpage desktop
				</a><br>


				<a data-fancybox data-type="iframe" data-src="https://legiaodosherois.uol.com.br/" href="javascript:;" class="mobile">
					Webpage mobile
				</a>
			
			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

	<script>
		$("[data-fancybox]").fancybox({
		    iframe : {
		        css : {
		            width : '1200px'
		        }
		    }
		});
		$("[data-fancybox]").fancybox({
		    iframe : {
		        css : {
		            width : '900px'
		        }
		    }
		});
		$("[data-fancybox]").mobile({
		    iframe : {
		        css : {
		            width : '400px'
		        }
		    }
		});
	</script>

</body>