<?
$h1         = 'Contato';
$title      = 'Contato';
$desc       = 'Entre em contato e envie sua mensagem pelo formulário e logo entraremos em contato. Qualquer dúvida estamos a disposição pelo email ou telefone';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Contato';
include('inc/head.php');
?>
<!--SweetAlert Modal-->
<link rel="stylesheet" href="js/sweetalert/css/sweetalert.css"/>
<script src="js/sweetalert/js/sweetalert.min.js"></script>
<!--/SweetAlert Modal-->


</head>

<body>
	<?include('inc/header2.php') ?>
	
	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<!-- Map Cleanup -->
			<?php
				// $mapa = str_replace("<iframe src=\"", "", $mapa);
    			// $mapa = str_replace("\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>", "", $mapa);
			?>
			<!-- End Map Cleanup -->
			<article>
				<h3 class="my-3 pb-5">Entre em contato conosco</h3>

				<?php
				if ($_POST)
				{
					//Carrega as classes do PHPMailer
					include("phpmailer/class.phpmailer.php");
					include("phpmailer/class.smtp.php");

					//envia o e-mail para o visitante do site
					$mailCopia = $_POST['txtEmail'];
					$nome = $_POST['txtNome'];
					$telefone = $_POST['txtTelefone'];
					$celular = $_POST['txtCelular'];
					$anexo   = $_FILES['anexo'];
					$endereco = $_POST['txtEndereco'];
					$cidade = $_POST['txtCidade'];
					$estado = $_POST['txtEstado'];
					$cep = $_POST['txtCEP'];

					$assunto = "Olá, recebemos sua mensagem!";
					$mensagem = "Obrigado pela sua mensagem, $nome!<br/>
								Em breve entraremos em contato.</br>";

					include("envioCliente.php");

					//envia o e-mail para o administrador do site
					//$mailCopia = '$emailContato';
					
					$nome = '$nomeSite';
					$assunto = "Nova mensagem no site $nomeSite";
					$mensagem = "<h3>Recebemos uma nova mensagem no site</h3>
					<br><br>

					<strong>Nome:</strong> $_POST[txtNome]<br/>
					<strong>E-mail:</strong> $_POST[txtEmail]<br/>
					<strong>Telefone:</strong> $_POST[txtTelefone]<br/>
					
					<strong>Endereço:</strong> $_POST[txtEndereco]<br/>
					<strong>Cidade:</strong> $_POST[txtCidade]<br/>
					<strong>Estado:</strong> $_POST[txtEstado]<br/>
					<strong>Cep:</strong> $_POST[txtCEP]<br/>
					<strong>Mensagem:</strong> $_POST[txtMensagem]";
					
					include("envioEmpresa.php");

					}
				?>

				<div class="row">
					
					<div class="col-md-7 px-4">

						<form enctype="multipart/form-data" method="POST" name="formContato" class="needs-validation" novalidade>

							<div class="form-row">
								<div class="form-group col-md">
									<!-- <label for="nome">Nome: </label> -->
									<input type="text" name="txtNome" placeholder="Seu nome" class="form-control" required>
									<div class="invalid-feedback">Informe seu nome</div>
								</div>
							</div>

							<div class="form row">
								<div class="form-group col-md">
									<!-- <label for="email">E-mail</label> -->
									<input type="email" name="txtEmail" placeholder="Informe o seu e-mail" class="form-control" required>
									<div class="invalid-feedback">Informe seu e-mail</div>
								</div>

								<div class="form-group col-md">
									<!-- <label for="tel">Telefone</label> -->
									<input type="number" name="txtTelefone" placeholder="Telefone" class="form-control" required>
									<div class="invalid-feedback">Informe seu telefon</div>
								</div>

								<!-- <div class="form-group col-md">
									<label for="cel">Celular</label>
									<input type="number" name="txtCelular" id="cel" placeholder="Celular" class="form-control">
								</div> -->
							</div>

							<!-- <div class="form-group">
								<label for="anexo">Adicionar arquivo</label>
								<input type="file" class="form-control-file" id="anexo" name="anexo">
							</div> -->

							<div class="form-group">
								<!-- <label for="end">Endereço</label> -->
								<input type="text" name="txtEndereco" placeholder="Endereço" class="form-control" required>
								<div class="invalid-feedback">Informe seu endereço</div>
							</div>
						
							<div class="form-row">
								<div class="form-group col-md-6">
									<!-- <label for="cidade">Cidade</label> -->
									<input type="text" name="txtCidade" placeholder="Cidade" class="form-control" required>
									<div class="invalid-feedback">Informe sua cidade</div>
								</div>
								<div class="form-group col-md">
									<!-- <label for="estado">Estado</label> -->
									<input type="text" name="txtEstado" placeholder="Estado" class="form-control" required>
									<div class="invalid-feedback">Escolha o seou estado</div>
								</div>
								<div class="form-group col-md">
									<!-- <label for="cep">CEP</label> -->
									<input type="tnumber" name="txtCEP" placeholder="CEP" class="form-control" required>
									<div class="invalid-feedback">Informe um CEP válido	</div>
								</div>
							</div>

							<div class="form-group">
								<!-- <label>Deixe sua mensagem:</label> -->
								<textarea rows="6" name="txtMensagem" placeholder="Mensagem..." class="form-control" required></textarea>
							</div>
							
							<br>

							<button type="submit" class="btn btn-large btn-primary">Enviar</button>
						
						</form>

						<p class="my-4">Os campos com * são obrigatórios</p>

					</div>
					
					<div class="col-md-5 px-4">
						<img src="imagens/logo.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>" class="img-fluid d-block mx-auto mb-5">

			            <h5 class="text-center mb-5"><?= $nomeSite . " <br> " . $slogan ?></h5>
	
			            <!-- <strong class="h6"><?= $rua . " - " . $bairro . "<br>" . $cidade . "-" . $UF . " - " . $cep ?></strong><br><br> -->
			            <strong class="h6"><i class="fas fa-phone"></i><?=" (".$ddd.") " . $fone ?></strong>
			            
			            <?php
				            // if (isset($fone2) && !empty($fone2)):
				            // echo "/ <strong class=/h6/> $ddd $fone2 </strong>";
				            // endif;
				            // if (isset($fone3) && !empty($fone3)):
				            // echo "/ <strong class=/h6/>$ddd $fone3</strong>";
				            // endif;
			            ?>
			            <br>
	
			            <strong class="h6"><i class="fas fa-envelope"></i> <?=" ".$emailContato?></strong><br><br>
			            <img src="<?=$url?>imagens/banner-ctt.jpg" alt="<?=$h1?>" title="<?=$h1?>" class="img-fluid">
			        </div>

				</div>

			</article>
		</section>	
        <!-- <iframe src="<?=$mapa?>" height="350" style="border:0; width: 100%;"></iframe> -->
	</main>

	<!-- Modal HTML Sucesso -->
	<div id="myModalSucess" class="modal fade">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h4 class="modal-title">Sucesso </h4>
	            </div>
	            <div class="modal-body">
	                <p>Tarefas realizadas com sucesso. </p>
	                <p class="text-warning"><small>Formulário enviado com sucesso</small></p>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">Close</button>
	            </div>
	        </div>
	    </div>
	</div> 

	<?include('inc/footer.php') ?>

</body>