<?
$h1         = 'Contador';
$title      = 'Contador';
$desc       = 'Contador';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Contador';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h3 class="my-4">Exemplo de contador</h3>

				<div class="jumbotron jumbotron-fluid py-5">
				  <div class="container">
				    <div class="row py-5">
				      <div class="col-12 col-md text-center">
				        <span class="count display-3">40</span>
				        <p>Lorem ipsum dolor.</p>
				      </div>
				      <div class="col-12 col-md text-center">
				        <span class="count display-3">5000</span>
				        <p>Lorem ipsum dolor.</p>
				      </div>
				      <div class="col-12 col-md text-center">
				        <span class="count display-3">20</span>
				        <p>Lorem ipsum dolor.</p>
				      </div>
				      <div class="col-12 col-md text-center">
				        <span class="count display-3">300</span>
				        <p>Lorem ipsum dolor.</p>
				      </div>
				    </div>
				  </div>
				</div>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

	<script>
	  $('.count').each(function () {
	    $(this).prop('Counter',0).animate({
	        Counter: $(this).text()
	    }, {
	        duration: 4000,
	        easing: 'swing',
	        step: function (now) {
	            $(this).text(Math.ceil(now));
	        }
	    });
	});
	</script>

</body>