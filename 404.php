<?
$h1         = 'Erro 404';
$title      = 'Erro 404';
$desc       = 'Erro 404';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Erro 404';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header2.php') ?>

	<main>
		<section class="container pt-3 pb-4 text-center img-404">
			<?=$breadcrumb?>			
			<h1 class="h3 my-5"><?=$h1?>!</h1>
			<article class="py-3">
				<h3 class="my-3">Ops! Página não encontrada</h3>

				<p>Navegue pelo site da <?=$nomeSite?> e encontre o que está procurando, escolha abaixo a página que deseja visualizar.</p>

				<div class="mt-5">
                    <h3>O que deseja fazer?</h3>
                    <ul class="list-inline text-right text-center">
						<li class="list-inline-item"><a rel="nofollow" title="Voltar a página inicial" href="<?=$url;?>" >Voltar a página inicial</a></li>
						<li class="list-inline-item"><a rel="nofollow" title="Ver O Mapa do site" href="<?=$url;?>mapa-site" >Ver O Mapa do site</a></li>
					</ul>
                </div>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>