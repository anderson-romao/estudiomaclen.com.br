<?
$h1         = 'Saiba mais';
$title      = 'Saiba mais';
$desc       = 'Saiba mais';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Saiba mais';
include('inc/head.php');
?>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			
			<div class="row">
				<article class="col-12 col-md-8 clearfix">
					<h3 class="my-4">Saiba mais do que oferecemos</h3>
				
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum voluptatum quae soluta, maiores fugiat doloribus eum, consequuntur est incidunt, harum quam dicta asperiores illum illo error reiciendis accusantium ducimus id dolores maxime eos quas ipsa. Esse porro, vero dolor aliquid pariatur est delectus exercitationem similique cum laudantium molestias. Dolorum maxime explicabo nemo iusto pariatur officia aspernatur sint, veritatis ex, omnis ipsam quia alias dolores incidunt iure laboriosam labore vero accusamus doloremque architecto reprehenderit praesentium libero rerum aut! Dolore quod cupiditate totam a. Dignissimos ipsum sapiente veritatis totam esse iure hic eaque voluptatem porro earum impedit nulla minus quasi unde similique, fugiat ipsam adipisci, eveniet voluptas aut! Nam, modi commodi officiis eos consequuntur quam delectus, ea sapiente, quia quo quibusdam tempore deleniti nostrum perferendis, nemo qui eum iste! Doloribus, illum sapiente labore nostrum error. Totam porro voluptate nam voluptatum illo, minus pariatur blanditiis perspiciatis eveniet aliquam iste animi odio, est ex consequatur, error explicabo dolores autem! Quam earum id, dolor veritatis sequi adipisci quidem molestiae error ducimus necessitatibus, iure unde aut placeat reprehenderit, delectus odit voluptatum. Pariatur amet in vero tenetur labore, nobis cumque voluptatum quia dignissimos, error temporibus vitae magni ullam autem, consequatur sed iusto quidem sapiente accusantium quam rem?</p>
					
					<p>Neque rerum doloribus quia quo iusto quas illo, eos. Quasi quisquam recusandae magnam, omnis nemo beatae commodi eos magni perferendis sit odit asperiores perspiciatis voluptatem iste quo quae, repudiandae? Culpa magni a et sint, ducimus atque velit, dolores cumque quae nihil quia maiores minus cupiditate provident odit eum. Consequatur blanditiis, odit obcaecati consequuntur atque. Beatae minus, reprehenderit excepturi aspernatur debitis dolor magni magnam facere, itaque facilis amet delectus? Voluptates, numquam dolores eos quos tempore, perferendis quisquam libero debitis. Dignissimos veniam accusamus velit, aliquid illum. Optio quo libero nostrum maiores, omnis, fugiat nisi quas pariatur molestiae accusantium animi veniam ex maxime error mollitia totam. Iusto velit provident non minus obcaecati eius, officiis eum blanditiis! Eius rerum magni autem doloremque sunt asperiores excepturi, nobis nostrum rem iste ducimus numquam, quibusdam similique id architecto cumque provident optio! Dolor vero iusto enim ipsum, natus quaerat rerum blanditiis eveniet nobis quam quo molestias vel. Quod eveniet laboriosam quam nesciunt vel accusantium ex. Deserunt suscipit incidunt asperiores temporibus natus ex dolore ducimus magni voluptate commodi praesentium adipisci, exercitationem, neque. Praesentium aperiam, qui deserunt molestias nisi illo impedit autem ut quidem enim fuga consectetur quaerat atque harum suscipit doloremque dolorum, adipisci id omnis? Dolores, beatae facilis maxime.</p>
				
				</article>
				<?include('inc/coluna-lateral.php');?>
			</div>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>