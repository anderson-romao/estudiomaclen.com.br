<?php
	require_once('phpmailer/PHPMailerAutoload.php');

	include('inc/dados.php');

	$mail = new PHPMailer();

    //E-mail para o cliente
    
    $mail->IsSMTP(); // envia por SMTP 
	$mail->CharSet = 'UTF-8';
	//$mail->True;
	$mail->Host = $ServidorSMTP; // SMTP servers
	$mail->SMTPSecure = 'tls'; // conexão segura com TLS
	$mail->Port = 587; 
	$mail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação
	$mail->Username = $emailContato; // SMTP username
	$mail->Password = $senha; // SMTP password
	$mail->From = $emailContato; // From

	$mail->FromName = $nomeSite ; // Nome de quem envia o email
	$mail->AddAddress($mailCopia, $nome); // Email e nome de quem receberá //Responder
	//$mail->AddReplyTo($mailCopia, $nome); // Cópia

	$mail->WordWrap = 50; // Definir quebra de linha
	$mail->IsHTML = (true); // Enviar como HTML
	$mail->Subject = $assunto ; // Assunto
	$mail->Body = '<br/>' . $mensagem . '<br/>' ; //Corpo da mensagem caso seja HTML
	$mail->AltBody = "$mensagem" ; //PlainText, para caso quem receber o email não aceite o corpo HTML

	$mail->AddEmbeddedImage('imagens/logo.png', $nomeSite); //Logo da empresa
	//$mail->AddAttachment($anexo['tmp_name'], $anexo['name']); //Anexo que o cliente envia

	// Envia o e-mail
	if(!$mail->send())
	{	
		echo "Erro no envio da mensagem ".$mail->ErrorInfo;
	}
	else
	{
		echo "<script>
		    swal({
		        title: 'Concluído!',
		        text: 'Fomulário enviado com sucesso!',
		        type: 'success',
		        showCancelButton: false,
		        confirmButtonClass: 'btn-success',
            	confirmButtonText: 'Ok',
            	closeOnConfirm: true
		    	},
			    function(){
			        location.href = '". $url ."';
			    }
			);
            </script>";
	}

	// echo "<script>window.location='contato.php';
	// alert('Ola $nome, sua mensagem foi enviada com sucesso!');</script>";

	$mail->ClearAllRecipients(); // Limpando os destinatários
    $mail->ClearAttachments(); // Limpando anexos
?>