<?php
    //E-mail com cópia para empresa

    $mail->IsSMTP(); // envia por SMTP 
	$mail->CharSet = 'UTF-8';
	//$mail->True;
	$mail->Host = $ServidorSMTP; // SMTP servers
	$mail->SMTPSecure = 'tls'; // conexão segura com TLS
	$mail->Port = 587; 
	$mail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação
	$mail->Username = $emailContato; // SMTP username
	$mail->Password = $senha; // SMTP password
	$mail->From = $emailContato; // From

	$mail->FromName = $nomeSite ; // Nome de quem envia o email
	$mail->AddAddress($emailContato, $nome); // Email e nome de quem receberá //Responder
	//$mail->AddReplyTo($mailCopia, $nome); // Cópia

	$mail->WordWrap = 50; // Definir quebra de linha
	$mail->IsHTML = (true); // Enviar como HTML
	$mail->Subject = $assunto ; // Assunto
	$mail->Body = '<br/>' . $mensagem . '<br/>' ; //Corpo da mensagem caso seja HTML
	$mail->AltBody = "$mensagem" ; //PlainText, para caso quem receber o email não aceite o corpo HTML

	//$mail->AddEmbeddedImage('imagens/logo.png', $nomeSite); //Logo da empresa
	$mail->AddAttachment($anexo['tmp_name'], $anexo['name']); //Anexo que o cliente envia

	// Envia o e-mail
	if(!$mail->send())
	{	
		echo "Erro no envio da mensagem ".$mail->ErrorInfo;
	}

	$mail->ClearAllRecipients(); // Limpando os destinatários
    $mail->ClearAttachments(); // Limpando anexos
?>