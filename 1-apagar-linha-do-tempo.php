<?
$h1         = 'Linha do tempo';
$title      = 'Linha do tempo';
$desc       = 'Linha do tempo';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Linha do tempo';
include('inc/head.php');
?>
<style>
	ul.timeline {
		list-style-type: none;
		position: relative;
	}
	ul.timeline:before {
		content: ' ';
		background: #d4d9df;
		display: inline-block;
		position: absolute;
		left: 29px;
		width: 2px;
		height: 100%;
		z-index: 400;
	}
	ul.timeline > li {
		margin: 20px 0;
		padding-left: 20px;
	}
	ul.timeline > li:before {
		content: ' ';
		background: white;
		display: inline-block;
		position: absolute;
		border-radius: 50%;
		border: 3px solid #22c0e8;
		left: 20px;
		width: 20px;
		height: 20px;
		z-index: 400;
	}
</style>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
			
				<h2 class="h3 my-4"><?=$h1?> Vertical</h2>

				<ul class="timeline">
					<li>
						<a target="_blank" href="https://www.totoprayogo.com/#">New Web Design</a>
						<a href="#" class="float-right">21 March, 2014</a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
					</li>
					<li>
						<a href="#">21 000 Job Seekers</a>
						<a href="#" class="float-right">4 March, 2014</a>
						<p>Curabitur purus sem, malesuada eu luctus eget, suscipit sed turpis. Nam pellentesque felis vitae justo accumsan, sed semper nisi sollicitudin...</p>
					</li>
					<li>
						<a href="#">Awesome Employers</a>
						<a href="#" class="float-right">1 April, 2014</a>
						<p>Fusce ullamcorper ligula sit amet quam accumsan aliquet. Sed nulla odio, tincidunt vitae nunc vitae, mollis pharetra velit. Sed nec tempor nibh...</p>
					</li>
				</ul>


			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

</body>