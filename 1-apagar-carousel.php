<?
$h1         = 'Carousel';
$title      = 'Carousel';
$desc       = 'Carousel';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Carousel';
include('inc/head.php');

?>
<!-- Carousel clientes/parceiros, projetos,  -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
.slick-slide {
	margin: 0px 20px;
}
.slick-slider{
	position: relative;
	display: block;
	box-sizing: border-box;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-webkit-touch-callout: none;
	-khtml-user-select: none;
	-ms-touch-action: pan-y;
	touch-action: pan-y;
	-webkit-tap-highlight-color: transparent;
}
.slick-list{
	position: relative;
	display: block;
	overflow: hidden;
}
.slick-slide{
	display: none;
	float: left;
	height: 100%;
	min-height: 1px;
}
[dir='rtl'] .slick-slide{
	float: right;
} 
.slick-initialized .slick-slide{
	display: block;
}
.btn-slick-carousel-left,
.btn-slick-carousel-right{
	position: absolute;
	z-index: 1000;
	top: 45%;
}
.btn-slick-carousel-left{
	left: -15px;
}
.btn-slick-carousel-right{
	right: -15px;
}
</style>

</head>

<body>
	<?include('inc/header.php') ?>

	<main>
		<!-- <?=$breadcrumbEstilo?> -->
		<section class="container pt-3 pb-4">
			<?=$breadcrumb?>			
			<h1 class="my-3"><?=$h1?></h1>
			<article>
				<h2 class="my-4">Modelo 1</h2>

				<div class="customer-cards slider py-5">
					<div class="slide">
						<div class="card shadow-lg">
							<a href="<?=$url?>" title="ESCREVA_AQUI">
								<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							</a>
							<div class="card-body text-center">
								<h5 class="card-title">Lorem ipsum dolor.</h5>
								<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Saiba mais</a>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="card shadow-lg">
							<a href="<?=$url?>" title="ESCREVA_AQUI">
								<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							</a>
							<div class="card-body text-center">
								<h5 class="card-title">Lorem ipsum dolor.</h5>
								<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Saiba mais</a>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="card shadow-lg">
							<a href="<?=$url?>" title="ESCREVA_AQUI">
								<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							</a>
							<div class="card-body text-center">
								<h5 class="card-title">Lorem ipsum dolor.</h5>
								<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Saiba mais</a>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="card shadow-lg">
							<a href="<?=$url?>" title="ESCREVA_AQUI">
								<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							</a>
							<div class="card-body text-center">
								<h5 class="card-title">Lorem ipsum dolor.</h5>
								<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Saiba mais</a>
							</div>
						</div>
					</div>
					<div class="slide">
						<div class="card shadow-lg">
							<a href="<?=$url?>" title="ESCREVA_AQUI">
								<img src="http://via.placeholder.com/290x200" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="card-img-top">
							</a>
							<div class="card-body text-center">
								<h5 class="card-title">Lorem ipsum dolor.</h5>
								<a href="<?=$url?>#" title="ESCREVA_AQUI" class="btn btn-primary">Saiba mais</a>
							</div>
						</div>
					</div>
				</div>
				
				<hr class="bg-secondary my-5">

				<h2 class="my-4">Modelo 2 (com fancybox)</h2>

				<div class="customer-imagens slider py-5">
					<div class="slide">
						<a href="imagens/time/integrante-01.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-01.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-02.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-02.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-03.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-03.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-04.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-04.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-05.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-05.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-06.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-06.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-07.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-07.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-08.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-08.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
					<div class="slide">
						<a href="imagens/time/integrante-09.jpg" title="ESCREVA_AQUI" data-toggle="lightbox">
							<img src="imagens/time/integrante-09.jpg" alt="ESCREVA_AQUI" title="ESCREVA_AQUI" class="img-fluid">
						</a>
					</div>
				</div>

				<hr class="bg-secondary my-5">

				<h2 class="my-4">Modelo 3 (depoimentos)</h2>

			</article>
		</section>	
	</main>

	<?include('inc/footer.php') ?>

	<!-- Carousel clientes/parceiros, projetos,  -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

	<script>
		//Modelos 1
		$(document).ready(function(){
			$('.customer-cards').slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 3000,
				arrows: true,
				dots: false,
				prevArrow:'<i class="fas fa-angle-left fa-2x btn-slick-carousel-left"></i>',
				nextArrow:'<i class="fas fa-angle-right fa-2x btn-slick-carousel-right"></i>',
				pauseOnHover: false,
				responsive: [{
					breakpoint: 768,
					settings: {
						slidesToShow: 1
					}
				}, {
					breakpoint: 520,
					settings: {
						slidesToShow: 1
					}
				}]
			});
		});

		//Modelo 2
		$(document).ready(function(){
		    $('.customer-imagens').slick({
		        slidesToShow: 6,
		        slidesToScroll: 1,
		        autoplay: true,
		        autoplaySpeed: 3000,
		        arrows: true,
		        dots: false,
		        prevArrow:'<i class="fas fa-angle-left fa-2x btn-slick-carousel-left"></i>',
		        nextArrow:'<i class="fas fa-angle-right fa-2x btn-slick-carousel-right"></i>',
		        pauseOnHover: false,
		        responsive: [{
		            breakpoint: 768,
		            settings: {
		                slidesToShow: 2
		            }
		        }, {
		            breakpoint: 520,
		            settings: {
		                slidesToShow: 2
		            }
		        }]
		    });
		});
	</script>

	<script>
		//lightbox
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: false,
				wrapping: false,
			});
		});
	</script>

</body>